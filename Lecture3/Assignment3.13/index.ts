/*## Assignment 3.13 How many days
Create a program that takes in a number from commandline that represents month of the year.
Use ``console.log`` to show how many days there are in the given month number.
*/

//käyttäjän vastuulla antaa luku

const kk: number = Number(process.argv[2]) % 12

let days = 31

if ([4, 6, 9, 11].includes(kk)) {
    days = 30
} else if (kk == 2) {
    days = 28 //oletetaan, ettei ole kyse karkausvuodesta
}

console.log(`There are ${days} days in the corresponding month.`)
