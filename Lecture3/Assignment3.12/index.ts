/*## Assignment 3.12 Largest and smallest
From the command line read in three numbers, number1, number2 and number3. Decide their values freely.

Find the  
a) largest one  
b) smallest one  
c) if they all are equal, print that out  

console.log() its name and value.*/

//päätin, että on käyttäjän vastuulla taas antaa kolme lukua

const number1: number = Number(process.argv[2])
const number2: number = Number(process.argv[3])
const number3: number = Number(process.argv[4])

if (number1 == number2 && number1 == number3) {
    console.log(`The three given numbers were all equal with the value ${number1}.`)
} else {
    if (number1 >= number2 && number1 >= number3) {
        console.log(`The first number is the largest and its value is ${number1}.`)
        if (number2 <= number3) {
            console.log(`The second number is the smallest and its value is ${number2}.`)
        } else {
            console.log(`The third number is the smallest and its value is ${number3}.`)
        }
    } else if (number2 >= number1 && number2 >= number3) {
        console.log(`The second number is the largest and its value is ${number2}.`)
        if (number1 <= number3) {
            console.log(`The first number is the smallest and its value is ${number1}.`)
        } else {
            console.log(`The third number is the smallest and its value is ${number3}.`)
        }
    } else if (number3 >= number1 && number3 >= number2) {
        console.log(`The third number is the largest and its value is ${number3}.`)
        if (number1 <= number2) {
            console.log(`The first number is the smallest and its value is ${number1}.`)
        } else {
            console.log(`The second number is the smallest and its value is ${number2}.`)
        }
    }
}


/* tässä versio joka tuli ennen kuin huomasin myös muuttujan nimen tulostustarpeen, ihan vain itselleni säästän tämänkin
let number1: number = Number(process.argv[2])
let number2: number = Number(process.argv[3])
let number3: number = Number(process.argv[4])

if(number1>number2){
    [number1,number2] = [number2,number1]   
}
if(number2>number3){
    [number2,number3] = [number3,number2]   
}
if(number1>number2){
    [number1,number2] = [number2,number1]   
}
if(number1===number3){
    console.log('The three given numbers were all equal.')
}else{
    console.log(`The largest number is ${number3} and the smallest number is ${number1}.`)
}*/