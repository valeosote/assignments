/*## Assignment 3.11: Greater, smaller or equal
1. Create a program that takes in two numbers a and b from the command line.
2. Print out "a is greater" if a is bigger than b, and vice versa, and "they are equal" if they are equal
3. Modify program to take in a third string argument c, and print out "yay, you guessed the password", if a and b are equal AND c is "hello world"
*/

//päätin, että on käyttäjän vastuulla aina antaa vähintään kaksi lukua enkä siis tarkista tapahtuiko näin oikeasti

let a: number = Number(process.argv[2])
let b: number = Number(process.argv[3])
// ^ const
const c = process.argv.length > 3 ? process.argv[4] : ''
// Tähän on itse asiassa modernissa JS:ssä syntaksi
// const c = process.argv[4] ?? ''

/* 2. kohta pelkästään
if(a===b){
    console.log('they are equal')
}else{
    if (a>b){
        [a,b] = [b,a]    
    }
    console.log(`${b} is greater`)
}*/


if (a === b) {
    if (c === 'hello world') {
        console.log('yay, you guessed the password')
    } else {
        console.log('they are equal')
    }
} else {
    if (a > b) {
        [a, b] = [b, a]
    }
    console.log(`${b} is greater`)

    // Näkisin, että tän vois ilmaista selkeämmin esim
    // const greater = a > b ? a : b
    // tai ihan vaan
    // if (a > b) {
    //     console.log(...)
    // } else {
    //     cosole.lot(...)
    // }
}
