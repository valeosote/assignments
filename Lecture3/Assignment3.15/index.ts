/*## Assignment 3.15 A Initial letters
Create a program that takes in 3 names and outputs only initial letters of those name separated with a dot.

example: ``node .\initialLetters.js Jack Jake Mike`` -> ``j.j.m``

## Assignment 3.15 B String length comparison
Create a program that takes in 3 names, and compares the length of those names.
Print out the names ordered so that the longest name is first.

example: ``node .\lengthcomparison.js Maria Joe Philippa`` -> ``Philippa Maria Joe``*/

console.log(`To review these assignments, please use the two commands:
    \n node ./dist/initialLetters.js Jack Jake Mike
    \n node ./dist/lengthcomparison.js Maria Joe Philippanode`)