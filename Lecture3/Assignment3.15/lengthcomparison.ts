/*## Assignment 3.15 B String length comparison
Create a program that takes in 3 names, and compares the length of those names. Print out the names ordered so that the longest name is first.

example: ``node .\lengthcomparison.js Maria Joe Philippa`` -> ``Philippa Maria Joe``*/

let nimi1 = process.argv[2]
let nimi2 = process.argv[3]
let nimi3 = process.argv[4]

if(nimi1.length<nimi2.length){
    [nimi1,nimi2]=[nimi2,nimi1]
}
if(nimi2.length<nimi3.length){
    [nimi2,nimi3]=[nimi3,nimi2]
}
if(nimi1.length<nimi2.length){
    [nimi1,nimi2]=[nimi2,nimi1]
}
console.log(`${nimi1} ${nimi2} ${nimi3}`)

// Ei helppolukuisin ratkaisu :)