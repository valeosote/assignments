/*## Assignment 3.15 A Initial letters
Create a program that takes in 3 names and outputs only initial letters of those name separated with a dot.

example: ``node .\initialLetters.js Jack Jake Mike`` -> ``j.j.m``*/

const name1 = process.argv[2]
const name2 = process.argv[3]
const name3 = process.argv[4]

console.log(`${name1[0].toLowerCase()}.${name2[0].toLowerCase()}.${name3[0].toLowerCase()}`)