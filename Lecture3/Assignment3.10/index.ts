let annettu: string = process.argv[2] || 'en'

const sanottavaa: { [key: string]: string } = {
    'en': 'Hello World',
    'es': 'Hola Mundo',
    'fi': 'Moikkelis vaa Maalima'
}

if (!Object.keys(sanottavaa).includes(annettu)){
    annettu='en'
}

console.log(sanottavaa[annettu])