/*## Assignment 3.14 ATM
Create a ATM program to check your balance.
Create variables ``balance``, ``isActive``, ``checkBalance``. Write conditional statement that implements the flowchart below.

![](atm-flowchart.png "ATM flowchart")

Change the values of `balance`, `checkBalance`, and `isActive` to test your code! */

const balance = -10
const isActive = true
const checkBalance = true

if (!checkBalance) {
    console.log('Have a nice day!')
} else if (isActive && balance > 0) {
    console.log(`Your balance is ${balance}.`)
} else if (!isActive) {
    console.log('Your account is not active.')
} else if (balance < 0) {
    console.log('Your balance is negative.')
} else { // tapaus balance===0
    console.log('Your account is empty.')
}
