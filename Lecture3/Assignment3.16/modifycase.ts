/*## Assignment 3.16: Modify case
Create a program that takes in a string, and modifies the every letter of that string to upper case or lower case,
depending on the input

example: ``node .\modifycase.js lower "Do you LIKE Snowmen?"`` -> ``do you like snowmen``

example: ``node .\modifycase.js upper "Do you LIKE Snowmen?"`` -> ``DO YOU LIKE SNOWMEN``

**NOTE** remember to take in the 2nd parameter with quotation marks*/

console.log(process.argv[2]==='lower' ? process.argv[3].toLowerCase() : process.argv[3].toUpperCase())

//tein nyt näin kun ei ollut tarkemmin kiellettykään
// Ei ollu kielletty. Ehkä geneerisesti kannattaa tavoitella koodia, joka olisi selkeää, helppolukuistsa ja speksit täyttävää. 