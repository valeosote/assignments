// 3.8

let n = 17
let sum = 0

while (n > 0) {
    if (n % 3 === 0 || n % 5 === 0) {
        sum += n
    }
    n -= 1
}
console.log(sum)

