const arr = ['banaani', 'omena', 'mandariini', 'appelsiini', 'kurkku', 'tomaatti', 'peruna']

//1. Print the 3rd and 5th items of the array and the array’s length
console.log('1: ' + arr[2] + ' ' + arr[4] + ' ' + arr.length)

//2. Then sort the array in alphabetical order and print the entire array
arr.sort()
console.log('2: ' + arr)

//3. Finally, add the item “sipuli” to the array, and print out again
arr.push('sipuli')
console.log('3: ' + arr)


//4. Remove the first item in the array, and print out again. HINT: shift()
arr.shift()
console.log('4: ' + arr)

//5. Print out every item in this array using .forEach()
console.log('5:')
arr.forEach((fruit) => { console.log(fruit) }) // i tarvitse aaltosulkeita

//6. Print out every item that contains the letter ‘r’. HINT: includes()
console.log('6:')
arr.forEach((fruit) => {
    if (fruit.includes('r')) {
        console.log(fruit)
    }
})