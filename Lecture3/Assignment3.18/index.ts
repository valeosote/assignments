/*## Assignment 3.18: Replace characters (difficult?)
Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.

example: ``node .\replacecharacters.js g h "I have great grades for my grading"`` -> ``I have hreat hrades for my hradinh``

* Hint: https://www.w3schools.com/jsref/jsref_replace.asp*/

const given1 = process.argv[2][0]
const given2 = process.argv[3][0]
let givenS = process.argv[4]

if (given1 != given2){
    while(givenS.includes(given1)){
        givenS = givenS.replace(given1,given2)
    }
}

console.log(givenS)

// Mitä jos given1 on sama kuin given2? => sit ollaan hetki tuossa luupissa :-P (okei, laitetaan siihen tarkistus, good catch)
