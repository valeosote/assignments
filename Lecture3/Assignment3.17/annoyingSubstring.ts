/*## Assignment 3.17: Annoying substring
Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.

example: ``node .\annoyingSubstring.js "Hey I'm alive!"`` -> ``Hey I'm`` */

let käyttäjältä = process.argv[2].trim()

käyttäjältä = käyttäjältä.substring(0,käyttäjältä.lastIndexOf(' '))

console.log(käyttäjältä)