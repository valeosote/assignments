/*## Assignment 3.17: Annoying substring
Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.

example: ``node .\annoyingSubstring.js "Hey I'm alive!"`` -> ``Hey I'm`` */

console.log(`Please use the format \n node ./dist/annoyingSubstring.js "Hey I'm alive!"`)