// 3.6 1-6

let tulos1 = '1. '
for( let i=0; i<=1000 ; i+=100 ){
    tulos1 += i + ' '
}
console.log(tulos1)

let tulos2 = '2. '
for( let i=1; i<=128 ; i*=2 ){
    tulos2 += i + ' '
}
console.log(tulos2)

let tulos3 = '3. '
for( let i=3; i<=15 ; i+=3 ){
    tulos3 += i + ' '
}
console.log(tulos3)

let tulos4 = '4. '
for( let i=9; i>=0 ; i--){
    tulos4 += i + ' '
}
console.log(tulos4)

let tulos5 = '5. '
for( let i=1; i<=4 ; i++ ){
    tulos5 += i + ' ' + i + ' ' + i + ' '
}
console.log(tulos5)

let tulos6 = '6. '
for( let i=0; i<=14 ; i++ ){
    tulos6 += i%5 + ' '
}
console.log(tulos6)