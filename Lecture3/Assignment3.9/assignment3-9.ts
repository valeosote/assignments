// 3.9 FizzBuzz

const m = 100

for (let i = 1; i <= m; i++) {
    let tulos = ''
    // tulosta ei tarvita tämän scopen ulkopuolella, joten se kannattaa määritellä täällä
    if (i % 3 === 0) {
        tulos += 'Fizz'
    }
    if (i % 5 === 0) {
        tulos += 'Buzz'
    }
    if (tulos === '') {
        tulos += i
    }
    console.log(tulos)
}

