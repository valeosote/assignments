import 'dotenv/config'
import express from 'express'
import productsRouter from './productsRouter'
import { createProductsTable } from './db'

const server = express()
server.use(express.json())

export default server

// createProductsTable()

server.use('/products', productsRouter)

const { PORT } = process.env || 3001
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})