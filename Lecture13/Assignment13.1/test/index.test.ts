import { jest } from '@jest/globals'
import { pool } from '../src/db'
import request from 'supertest'
import server from '../src/index'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /products', () => {
    const mockResponse = {
        rows: [
            { id: 101, name: 'Test Item 1', price: 100 },
            { id: 102, name: 'Test Item 2', price: 200 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    
    it('dummy test', () => {
        expect(true).toBe(true)
    })

    it('goes to GET products', async () => {
        const response = await request(server)
            .get('/products')

        expect(response.status).toBe(200)
        expect(response.body).toEqual(mockResponse.rows)    
    })

})
