import express, { Request, Response } from 'express'
import { getCommentsByUser, addComment, delComment } from './commentsDao'

const router = express.Router()

router.get('/:userID', async (req: Request, res: Response) => {
    const user_id = Number(req.params.userID)
    const userComments = await getCommentsByUser(user_id)
    userComments.length === 0
        ? res.status(404).send('No comments found for this user')
        : res.status(200).send(userComments)
})

router.post('/', async (req: Request, res: Response) => {
    const { user_id, post_id, content } = req.body

    if (!user_id || !post_id || !content) {
        return res.status(400).send('Please include proper data')
    }

    const newComment = await addComment(user_id, post_id, content)
    res.status(201).send(newComment)
})

router.delete('/:id', async (req: Request, res: Response) => {
    const comment_id = Number(req.params.id)
    await delComment(comment_id)
    res.status(204).send()
})

export default router
