import pg from 'pg'

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, NODE_ENV } = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: NODE_ENV === 'production' //muista muuttaa tätä sit kun on sen aika!
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = 'dbError'
        throw error
    } finally {
        client.release()
    }
}

export const queryAllTables = async () => {
    const query = `
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema = 'public';`
    await executeQuery(query)
    console.log('It is somewhat probable that something just happened somewhere')
}
