//import 'dotenv/config'
import express from 'express'
import helmet from 'helmet'
import usersRouter from './usersRouter'
import postsRouter from './postsRouter'
import commentsRouter from './commentsRouter'
import { logger, notFound } from './middleware'

const server = express()
server.use(helmet())

server.use(express.json())
server.use(logger)

server.use('/users', usersRouter)
server.use('/posts', postsRouter)
server.use('/comments', commentsRouter)
//server.use(express.static('public'))

server.use(notFound)

export default server