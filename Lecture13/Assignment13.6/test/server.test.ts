import request from 'supertest'
import { jest } from '@jest/globals'
import server from '../src/server'
import { pool } from '../src/db'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

const mockResponse = {
    users: [
        { username: 'testaaja1', fullname: 'test aaja', email: 'testemail@dot.com', user_id: 1 },
        { username: 'testaaja2', fullname: 'aaja test', email: 'testmemale@com.com', user_id: 999 }
    ],
    posts: [
        { user_id: 1, title: 'testausaihe', content: 'testaussisältö', post_id: 1 }
    ],
    comments: [
        { user_id: 1, post_id: 1, content: 'testauskommentti', comment_id: 1 }
    ]
}

beforeAll(() => {
    initializeMockPool(mockResponse)
})

afterAll(() => {
    jest.clearAllMocks()
})

test('dummy test, which I still like to keep around so at least one test passes', () => {
    expect(true).toBe(true)
})

describe('/users', () => {

    it('should post a new user', async () => {
        const newUser = { username: 'testuser', fullname: 'Test User', email: 'testaddress@email.com' }
        const response = await request(server)
            .post('/users').send(newUser)

        expect(response.status).toBe(201)
    })
    
    it('should fail to post a new user without enough info', async () => {
        const newUser = { username: 'testuser2' }
        const response = await request(server)
            .post('/users').send(newUser)

        expect(response.status).toBe(400)
        //expect(response.body).toBe('Missing info')
    })

    it('should fail to post the same user again', async () => {
        const newUser = { username: 'testuser', fullname: 'Testausta Käyttäjä', email: 'no@yes.com' }
        const response = await request(server)
            .post('/users').send(newUser)

        expect(response.status).toBe(400)
        //expect(response.body).toBe('Username already taken')
    })

    it('should get all users', async () => {
        const response = await request(server)
            .get('/users')

        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })

    it('should get a specific user by id', async () => {
        const response = await request(server)
            .get('/users/1')

        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })

    it('should not find anything for an invalid id number', async () => {
        const response = await request(server).get('/users/0')
        expect(response.status).toBe(404)
    })
    it('should delete a specific user by id', async () => {
        const response = await request(server).delete('/users/1')
        expect(response.status).toBe(204)
    })
    it('should not delete a specific user by invalid id', async () => {
        const response = await request(server).delete('/users/0')
        expect(response.status).toBe(404)
    })
})

describe('/posts', () => {
    it('should post a new post', async () => {
        const newPost = { user_id: 1, title: 'Test Title', content: 'Test Content' }
        const response = await request(server).post('/posts').send(newPost)
        expect(response.status).toBe(201)
    })
  
    it('should fail to post a new post without enough info', async () => {
        const newPost = { user_id: 1, title: 'Test Title' }
        const response = await request(server).post('/posts').send(newPost)
        expect(response.status).toBe(400)
        //expect(response.body).toBe('Please include proper data')
    })
  
    it('should get all posts', async () => {
        const response = await request(server).get('/posts')
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })
  
    it('should get a specific post by post id', async () => {
        const response = await request(server).get('/posts/1')
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })
  
    it('should not find anything for an invalid post id number', async () => {
        const response = await request(server).get('/posts/0')
        expect(response.status).toBe(404)
    })
  
    it('should delete a specific post by post id', async () => {
        const response = await request(server).delete('/posts/1')
        expect(response.status).toBe(204)
    })
  
    it('should not delete a specific post by invalid post id', async () => {
        const response = await request(server).delete('/posts/0')
        expect(response.status).toBe(404)
    })
})

describe('/comments', () => {
    it('should post a new comment', async () => {
        const newComment = { user_id: 1, post_id: 1, content: 'Test Comment Content' }
        const response = await request(server).post('/comments').send(newComment)
        expect(response.status).toBe(201)
    })
  
    it('should fail to post a new comment without enough info', async () => {
        const newComment = { user_id: 1, post_id: 1 }
        const response = await request(server).post('/comments').send(newComment)
        expect(response.status).toBe(400)
        //expect(response.body.error).toBe('Please include proper data')
    })
  
    it('should get all comments', async () => {
        const response = await request(server).get('/comments')
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })
  
    it('should get a list of comments by a specific user according to user_id', async () => {
        const response = await request(server).get('/comments/1')
        //console.log(response.body)
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })
  
    it('should not find comments for an invalid user_id', async () => {
        const response = await request(server).get('/comments/0')
        expect(response.status).toBe(404)
    })
  
    it('should delete a specific comment by comment id', async () => {
        const response = await request(server).delete('/comments/1')
        expect(response.status).toBe(204)
    })
  
    it('should not delete a specific comment by invalid comment id', async () => {
        const response = await request(server).delete('/comments/0')
        expect(response.status).toBe(404)
    })
})