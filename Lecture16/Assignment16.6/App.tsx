import { useState, useEffect, ChangeEvent } from 'react'
import axios from 'axios'

function App() {
  const url = 'https://cataas.com/cat'

  const [cat, setCat] = useState('')
  const [inputText, setInputText] = useState('')

  async function fetchData() {
    const response = await axios.get(url+'?json=true')
    console.log(response.data._id)
    inputText ? setCat(url+'/'+response.data._id+'/says/'+inputText) : setCat(url+'/'+response.data._id)
  }

  useEffect(() => {
    fetchData()
  }, [])  //eslint varoittaa tästä. löysin ohjeen, että voisi käyttää useCallbackiä tässä, mutta tuntui toimivan ilmankin

  const resetMe = () => {
    fetchData()
  }

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputText(event.target.value)
  }

  return (<div>
            <input type="text" onChange={handleInputChange} />
            <button onClick={resetMe}>Reload</button>
            <img src={cat} alt="Random Cat Pic"/>
        </div>
  )
}

export default App
