import { useState } from 'react'

function App() {

  const maksimiluku = 12

  const alussa: string[] = []
  for (let i = 1; i <= maksimiluku; i++){
    alussa.push(i.toString())
  }

  const [myList, setMyList] = useState<string[]>(alussa)
  const [myIndex, setMyIndex] = useState<number>(0)

  const onButtonClick = () => {
    const newest = Math.floor(Math.random()*(maksimiluku-myIndex))
    const newList: string[] = [...myList]

    //[newList[myIndex], newList[newest+myIndex]] = [newList[newest+myIndex], newList[myIndex]]
    const temp = newList[myIndex]
    newList[myIndex] = newList[newest+myIndex]
    newList[newest+myIndex] = temp
    //console.log(newest, myIndex, myList)

    setMyIndex(myIndex+1)
    setMyList([ ...newList])
  } //kun kaikki luvut on arvottu, listaan lisääntyy undefined kunnes resetoidaan

  const resetMe = () => {
    setMyIndex(0)
  }

  return (
    <div className='App'>
      <button onClick={resetMe}>Reset</button>
      <button onClick={onButtonClick}>Draw!</button>
        {myList.slice(0,myIndex).map( number => {
            return <div>
                {number}
            </div>
        })}
    </div>
  )
}


/* version 1.0 (drawn numbers may repeat)
function App() {

  const [myList, setMyList] = useState<string[]>([])

  const onButtonClick = () => {
    const newest = Math.floor(Math.random()*90+1).toString()

    setMyList([ ...myList, newest])
  }

  return (
    <div className='App'>
      <button onClick={onButtonClick}>Draw!</button>
        {myList.map( number => {
            return <div>
                {number}
            </div>
        })}
    </div>
  )
}*/

export default App