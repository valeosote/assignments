import { useState, useEffect } from "react"
import AddContact from './AddContact'
import EditContact from './EditContact'
import ShowContact from './ShowContact'
import { Contact } from './ContactList'

interface OtherColumnProps {
    showThisOne: number
    revealContact: string
    editContact: string
    sendUpstairs: (contact: Contact) => void
    sendUpLadder: (contact: Contact) => void
    letsGoToStart: () => void
    lestGoToStart: () => void
    letsGoEditAContact: () => void
    removeThisContact: (id: string) => void
    contactList: Contact[]
}

function Basic() {
    return(
        <h1>Contact Manager</h1>
    )
}

function OtherColumn({ showThisOne, revealContact, editContact, sendUpstairs, sendUpLadder, letsGoToStart, letsGoEditAContact, removeThisContact, contactList }: OtherColumnProps) {

    const [showing, setShowing] = useState(showThisOne)

    useEffect(() => {
        setShowing(showThisOne)
      }, [showThisOne])

    const tabs = [
        <Basic/>,
        <AddContact letsGoToStart={letsGoToStart} sendUpstairs={sendUpstairs}/>,
        <EditContact id={editContact} sendUpLadder={sendUpLadder} contactList={contactList} letsGoToStart={letsGoToStart}/>,
        <ShowContact removeThisContact={removeThisContact} letsGoEditAContact={letsGoEditAContact} contactId={revealContact} contactList={contactList}/>
    ]

    return (
        <div>
            {tabs[showing]}
        </div>
    )
}

export default OtherColumn
