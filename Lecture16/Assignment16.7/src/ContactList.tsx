import { useState, ChangeEvent } from 'react'

export interface Contact {
    name: string
    email?: string
    phone?: string
    address?: string
    website?: string
    info?: string
    id: string
  }

interface ContactListProps {
    showThisContact: (uuid: string) => void
    letsGoAddAContact: () => void
    contactList: Contact[]
}

function ContactList({showThisContact, letsGoAddAContact, contactList }: ContactListProps) {

    const [inputText, setInputText] = useState('')

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setInputText(event.target.value)
    }

    const handleSelectId = (uuid: string) => {
        //console.log(uuid)
        showThisContact(uuid)
    }

    return (
        <div>
            <input type="text" onChange={handleInputChange} />
            {contactList.filter( contact => contact.name.toLowerCase().includes(inputText.toLowerCase())).map( contact => {
                return( <div key={contact.id} onClick={() => handleSelectId(contact.id)}>
                    {contact.name}
                </div>
                )
            })}
            <br/>
            <button onClick={letsGoAddAContact}>Add a Contact</button>
        </div>
    )
}

export default ContactList