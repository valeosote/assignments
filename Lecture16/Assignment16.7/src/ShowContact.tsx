//import { useState } from "react"
import { Contact } from './ContactList'

interface ShowContactProps {
    removeThisContact: (id: string) => void
    letsGoEditAContact: () => void
    contactId: string
    contactList: Contact[]
}

function ShowContact({ removeThisContact, letsGoEditAContact, contactId, contactList }: ShowContactProps) {

    const contact: Contact = contactList.find(contact => contact.id === contactId) ?? {name: '0', id:'0'}

    return (
        <div>
            <h3>Contact Details</h3>
            <h2>{contact.name}</h2>
            {(contact.email === '') ? null : 
                <div key='email'>   
                    <em>email:</em> {contact.email}
                </div>
            }
            {(contact.phone === '') ? null : 
                <div key='phone'>
                    <em>phone:</em> {contact.phone}
                </div>
            }
            {(contact.address === '') ? null : 
                <div key='address'>
                    <em>address:</em> {contact.address}
                </div>
            }
            {(contact.website === '') ? null : 
                <div key='website'>
                    <em>website:</em> {contact.website}
                </div>
            }
            {(contact.info === '') ? null : 
                <div key='info'>
                    <em>info:</em> {contact.info}
                </div>
            }
            <button onClick={() => removeThisContact(contact.id)}>Remove</button>
            <button onClick={letsGoEditAContact}>Edit</button>
        </div>
    )
}

export default ShowContact