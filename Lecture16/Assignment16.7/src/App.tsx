import { useState } from 'react'
import ContactList, { Contact } from './ContactList'
import OtherColumn from './OtherColumn'

//by Miika Toukola
//toiminnat toimii, muut, kuten ulkoasu, olis toki parannettavissa

function App() {

  const [contactList, setContactList] = useState<Contact[]>([])
  const [whichTab, setWhichTab] = useState(0)
  const [uuid, setUuid] = useState('')

  const handleAddAContact = (contact: Contact) => {
    if(contact.name!==''){
        const newContact: Contact = contact
        setContactList([...contactList,newContact])
    }
  }
  const handleEditAContact = (incoming: Contact) => {
    const updatedContactList = contactList.map(contact =>
      contact.id === incoming.id ? { ...contact, ...incoming } : contact
    )
    setContactList(updatedContactList)
  }
  const handleShowContact = (newUuid: string) => {
    setUuid(newUuid)
    setWhichTab(3)
  }

  //nämä kolme selvästikin voisi yhdistää, mutta rakensin ne yksi kerrallaan niin näin tässä kävi... :-)
  const letsGoToStart = () => {
    setWhichTab(0)
  }
  const letsGoAddAContact = () => {
    setWhichTab(1)
  }
  const letsGoEditAContact = () => {
    setWhichTab(2)
  }
  const removeThisContact = (id: string) => {
    const updatedContactList = contactList.filter( contact => contact.id !== id)
    setContactList([...updatedContactList])
    setWhichTab(0)
  }

  return (
    <div style={{ display: 'flex' }}>
      <ContactList showThisContact={handleShowContact} letsGoAddAContact={letsGoAddAContact} contactList={contactList}/>
      <OtherColumn showThisOne={whichTab} sendUpstairs={handleAddAContact} sendUpLadder={handleEditAContact}
                   revealContact={uuid} editContact={uuid} letsGoToStart={letsGoToStart}
                   letsGoEditAContact={letsGoEditAContact} removeThisContact={removeThisContact} contactList={contactList}/> 
    </div>
  )
}

export default App
