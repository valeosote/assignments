import { useState } from "react"
import { Contact } from './ContactList'

interface EditContactProps {
    id: string
    sendUpLadder: (contact: Contact) => void
    contactList: Contact[]
    letsGoToStart: () => void
}

function EditContact({id, sendUpLadder, contactList, letsGoToStart}: EditContactProps) {
    
    const contact: Contact = contactList.find(contact => contact.id === id) ?? {name: '0', id:'0'}

    const [name, setName] = useState(contact.name)
    const [email, setEmail] = useState(contact.email)
    const [phone, setPhone] = useState(contact.phone)
    const [address, setAddress] = useState(contact.address)
    const [website, setWebsite] = useState(contact.website)
    const [info, setInfo] = useState(contact.info)

    const onSubmit = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        console.log('A contact was edited.')
        const newContact: Contact = { name, email, phone, address, website, info, id}
        sendUpLadder(newContact)
    }

    return (
        <div>
            <h2>Edit Contact</h2>
            <form onSubmit={onSubmit}>
                <label> Name:
                    <input type='text' value={name} onChange={e => { setName(e.target.value) }}/>
                </label>
                <br/>
                <label> E-mail: 
                    <input type='text' value={email} onChange={e => { setEmail(e.target.value) }}/>
                </label>
                <br/><label> Phone: 
                    <input type='text' value={phone} onChange={e => { setPhone(e.target.value) }}/>
                </label>
                <br/><label> Address: 
                    <input type='text' value={address} onChange={e => { setAddress(e.target.value) }}/>
                </label>
                <br/><label> Website: 
                    <input type='text' value={website} onChange={e => { setWebsite(e.target.value) }}/>
                </label>
                <br/><label> Info: 
                    <input type='text' value={info} onChange={e => { setInfo(e.target.value) }}/>
                </label>
                <input type='submit' value='Save' disabled={!name}/>
                <input type='button' value='Cancel' onClick={()=>letsGoToStart()}/>
            </form> 
        </div>
    )
}

export default EditContact