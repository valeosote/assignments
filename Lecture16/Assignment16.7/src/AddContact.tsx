import { useState } from "react"
import { Contact } from './ContactList'
import { v4 } from 'uuid'

interface AddContactProps {
    letsGoToStart: () => void
    sendUpstairs: (contact: Contact) => void
}

function AddContact({ letsGoToStart, sendUpstairs}: AddContactProps) {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [address, setAddress] = useState('')
    const [website, setWebsite] = useState('')
    const [info, setInfo] = useState('')

    const onSubmit = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        console.log('A new contact was submitted.')
        const id: string = v4()
        const newContact: Contact = { name, email, phone, address, website, info, id}
        sendUpstairs(newContact)
    }//ei tyhjennä kenttiä ihan vain kun se helpotti testaamista (sai siis useamman tyypin listaan nopeasti)

    return (
        <div>
            <h2>Add a Contact</h2>
            <form onSubmit={onSubmit}>
                <label> Name:
                    <input type='text' value={name} onChange={e => { setName(e.target.value) }}/>
                </label>
                <br/>
                <label> E-mail: 
                    <input type='text' value={email} onChange={e => { setEmail(e.target.value) }}/>
                </label>
                <br/><label> Phone: 
                    <input type='text' value={phone} onChange={e => { setPhone(e.target.value) }}/>
                </label>
                <br/><label> Address: 
                    <input type='text' value={address} onChange={e => { setAddress(e.target.value) }}/>
                </label>
                <br/><label> Website: 
                    <input type='text' value={website} onChange={e => { setWebsite(e.target.value) }}/>
                </label>
                <br/><label> Info: 
                    <input type='text' value={info} onChange={e => { setInfo(e.target.value) }}/>
                </label>
                <input type='submit' value='Save' disabled={!name}/>
                <input type='button' value='Cancel' onClick={()=>letsGoToStart()}/>
            </form> 
        </div>
    )
}

export default AddContact