import { useState } from 'react'

const FeedbackForm = () => {
  const [freeForm, setFreeForm] = useState('')
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [radio1, setRadio1] = useState('')
  //const [radio2, setRadio2] = useState('')
  //const [radio3, setRadio3] = useState('')

  const onSubmit = (e: { preventDefault: () => void }) => {
      e.preventDefault()
      console.log(radio1) //, radio2, radio3)
      console.log(freeForm)
      console.log(name, address)
  }

  const handleReset = () =>{
    setFreeForm('')
    setName('')
    setAddress('')
    setRadio1('')
  }

  //const isSubmitDisabled = !(radio1 && freeForm)

  return (
      <form onSubmit={onSubmit} onReset={handleReset}>
          {
          }<p>What do you have to say today???</p>
          <input type='radio' value='feedback' name='osuva' onChange={e => {setRadio1(e.target.value)}}/>
          <label>feedback</label>
          <br/>
          <input type='radio' value='question' name='osuva' onChange={e => {setRadio1(e.target.value)}}/>
          <label>question</label>
          <br/>
          <input type='radio' value='suggestion' name='osuva' onChange={e => {setRadio1(e.target.value)}}/>
          <label>suggestion</label>
          <br/>      
          <input type='text' value={freeForm} onChange={e => { setFreeForm(e.target.value) }} style={{width:300}}/>
          <br/>
          <label htmlFor='name'>Name:</label>
          <input type='text' value={name} id='name' onChange={e => { setName(e.target.value) }} /> 
          <br/>
          <label htmlFor='address'>Address:</label>
          <input type='text' value={address} id='address' onChange={e => { setAddress(e.target.value) }} />
          <br/>
          <input type='submit' value='submit' disabled={!(radio1 && freeForm)}/>
          <input type='reset' value='reset'/>
      </form> 
  )
}


function App() {
  return (
    <div className='App'>
      <FeedbackForm/>
    </div>
  )
}

export default App