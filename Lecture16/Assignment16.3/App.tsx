import axios from 'axios'
import { useState, useEffect } from 'react'

const App = () => {
  const url = 'https://api.api-ninjas.com/v1/dadjokes'
  const config = {
    headers: {
      'X-Api-Key': 'qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM'
    }
  }

  const [joke, setJoke] = useState('')

  useEffect(() => { // leave callback function as synchronous
   
      async function fetchData() { // declare async function inside callback
         
          const response = await axios.get(url, config)

          const data = response.data[0].joke //this line used await in the example

          setJoke(data) //hmm, vielä tulee kaksi vitsiä yhdestä updatesta...
      }
      fetchData(); // call the async function inside synchronous callback
  }, []) // set the second argument as [] to avoid loop from state change

  return <div>{joke}</div>
}

export default App