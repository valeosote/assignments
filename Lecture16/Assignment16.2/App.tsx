import { useState, useEffect } from "react"

function App() {

  const [time, setTime] = useState(0)

  useEffect( () => {
    const timeout = setTimeout( () => setTime(time+1), 1000)

    return () => clearTimeout(timeout)
  }, [time])


  const handleButtonClick = () => {
    setTime(0)
    console.log('button')
  }

  return (
    <div className='App'>
          <div>
            {time}
          </div>
          <button onClick={handleButtonClick}>press</button>
    </div>
  )
}

export default App