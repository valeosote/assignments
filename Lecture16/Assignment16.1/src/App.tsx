import { useState } from "react"
import InputField from './InputField'

function App() {

  const [myList, updateMyList] = useState<string[]>([])

  const addToMyList= (digits: string) => {
    const newList = [...myList, digits]
    updateMyList(newList)
  }

  return (
    <div className='App'>
      <InputField addThis= {addToMyList} />
      {myList.map(item => {
        return (
          <div>
            {item}
          </div>
        )
      })}
    </div>
  )
}

export default App