import { useState, ChangeEvent } from "react"

interface InputFieldType {
    addThis: (text: string) => void
}

function justDigits(digits: string): boolean {
    for(const i of digits){
        if(!'0123456789'.includes(i)){
            return false
        }
    }
    return true
}

function InputField(props: InputFieldType) {

    const [inputText, setInputText] = useState('')

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const newText = event.target.value
        if(justDigits(newText)) {
            setInputText(newText)
            if(newText.length > 9){
                props.addThis(newText)
                setInputText('')
            }
        }  
      }

    return (
        <input type='text' value={inputText} onChange={handleInputChange} />
    )
}

export default InputField
