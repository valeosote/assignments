import { useState } from 'react'

// Projektista puuttuu package.json joten sitä ei pysty ajamaan.

function App() {

  const buttonStyle = {
    width: 30,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
  // preferoidaan css-tiedostoja, jos ei ole erityistä tarvetta dynaamisille tyyleille

  const [tila, setTila] = useState('         ')
  // onko tän logiikka siis, että tila on 9 spacea, jotka korvataan sitten X/O merkeillä? Jos kyllä, niin silloin string on ihan väärä datatyyppi, käytä arraytä.

  const [merkki, setMerkki] = useState('X')

  const markButton = (i: number) => {
    if(tila[i]===' '){ // huomaa, kuinka tässä käytät stringiä jo kuten se olisi array
      const newTila = tila.substring(0, i) + merkki + tila.substring(i+1)
    //   const newTila = tila.map((t,idx) => idx === i ? merkki : t)
      setTila(newTila)
      merkki==='X' ? setMerkki('O') : setMerkki('X') // ehkä mielummin setMerkki(merkki === 'X' ? 'O' : 'X')
      if(!newTila.includes(' ')){
        setMerkki('Loppu!')
      }
    //   if (!newTila.some(t => t === '')) setMerkki('Loppu!')
    }
  }

  const resetButton = () => {
    setTila('         ')
    setMerkki('X')
  }

  return (
    <>
      <div className='App' style={{ display: 'grid', gridTemplateColumns: 'repeat(3, 30px)', gap: '5px' }}>
        <button onClick={() => markButton(0)} style={buttonStyle}>
                  {tila[0]}
        </button>
        <button onClick={() => markButton(1)} style={buttonStyle}>
                  {tila[1]}
        </button>
        <button onClick={() => markButton(2)} style={buttonStyle}>
                  {tila[2]}
        </button>
        <button onClick={() => markButton(3)} style={buttonStyle}>
                  {tila[3]}
        </button>
        <button onClick={() => markButton(4)} style={buttonStyle}>
                  {tila[4]}
        </button>
        <button onClick={() => markButton(5)} style={buttonStyle}>
                  {tila[5]}
        </button>
        <button onClick={() => markButton(6)} style={buttonStyle}>
                  {tila[6]}
        </button>
        <button onClick={() => markButton(7)} style={buttonStyle}>
                  {tila[7]}
        </button>
        <button onClick={() => markButton(8)} style={buttonStyle}>
                  {tila[8]}
        </button>
      </div>
      <div>
        <p>Vuoro: {merkki}</p>
        <button onClick={resetButton} style={{display:'flex'}}>reset</button>
      </div>
    </>
  )
}

export default App