import { useState, ChangeEvent } from "react"

const InputDisplay = () => {
  const [inputText, setInputText] = useState('')
  const [displayText, setDisplayText] = useState('')

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputText(event.target.value)
  }

  const handleButtonClick = () => {
    setDisplayText(inputText)
  }

  return (
    <div>
      <input type="text" onChange={handleInputChange} />
      <button onClick={handleButtonClick}>Accept</button>
      {<p>{displayText}</p>}
    </div>
  )
}

export default InputDisplay
