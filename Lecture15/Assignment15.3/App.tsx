function RandomNumber() {
    const element = <h1>Random Number Generator</h1>
    const randomnumber = Math.floor(Math.random() * 100) + 1
    return <div>
      {element}
      {randomnumber}
    </div>
}
  
function App() {
  return (
    <div className='App'>
      <RandomNumber />
    </div>
  )
}

export default App