
function Boldering() {
    const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]
    return(
        <div className='App'>
        {namelist.map((name, index) => {
            const tyylit = {
                fontWeight: index % 2 === 0 ? 'bold' : 'normal',
                fontStyle: index % 2 === 1 ? 'italic' : 'normal'
            }
            return <div key={index} style={tyylit}>
                {name}
            </div>
        })}
      </div>
    )
}

export default Boldering