import { useState } from "react"

interface counterProps {
    name: string
    updateTheTotal: () => void
}

 function Counter({ name, updateTheTotal }: counterProps) {

    const [number, setNumber] = useState(0)
    const growNumber = () => {
        setNumber(number+1)
        updateTheTotal()
        console.log('klikattiin' + name)
    }

    return (
        <div className='App'>
            <br />
            <button onClick={growNumber}>
                {number}
            </button>
        </div>
    )
 }

export default Counter
