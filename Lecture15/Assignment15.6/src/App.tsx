import { useState } from "react"
import Counter from './Counter'

function App() {
  const [total, setTotal] = useState(0);

  const updateTheTotal = () => {
    setTotal(total + 1)
  }
  
  return (
    <div>
      <h1>Counters</h1>
      <Counter name="eka" updateTheTotal={updateTheTotal}/>
      <Counter name="toka" updateTheTotal={updateTheTotal}/>
      <Counter name="kolmas" updateTheTotal={updateTheTotal}/>
      <p>{total}</p>
    </div>
  )
}

export default App