import { useState, ChangeEvent } from "react"

import ToDoItem from "./ToDoItem"

// pari kommenttia tästä toteutuksestani...
// toi lista on vähän hassu.
// Löysin ohjeen, että kannattaisi listassa mielummin pitää komponentin alustava info, ei itse komponenttia.
// Silti tein näin, koska en ihan ymmärtänyt miten komponentin sisäinen tila säilyy.
// Lisäksi, ihan toi luvun säilöminen listaan on oudon tuntuista, mutta kun kielettiin indexin käyttämistä.
// Eniten siis kesti mulla aikaa saada tämä komponentin poistaminen toimimaan.
// Siihen on varmaan parempikin tapa, mutta nyt tein näin ja sentään toimii.
// Muuten, olisi luontevampaa saada toi poistonappi komponentin sisään, mutta silti
// tieto sen poistosta piti kommunikoida tähän parenttiin, niin laitoin sen vain tähän tällä kertaa.

// Joo, tässä on nyt mennyt logiikka vähän väärällä tavalla. Ajatus on siis, että tilamuuttujaan tallennetaan data, jota käytetään komponentin renderöintiin. Tätä varten kannattaa luoda oma interface sellaisille elementeille, jotka koodin kontekstissa toistuu. Tässä tapauksessa todo-itemeille. Sitten kun meillä on lista todo-itemeitä (jotka on siis ihan objekteja), niin me luodaan komponentti, joka renderöi yksittäisen todo-itemin, ja käytetään niitä mäppäämään listan itemit komponenteiksi. Käy katsomassa mallivastaus ja vertaa siihen.

function App() {
  const [listOfItems, setList] = useState<{item: React.ReactNode, iid: number}[]>([])
  const [inputText, setNewItemText] = useState('')
  const [isid, setIsid] = useState(0) // tämä ei vaikuta näkymään, joten sen ei kannata olla tilamuuttuja

  const updateList = (event: ChangeEvent<HTMLInputElement>) => {
    setNewItemText(event.target.value)
  }

  const handleButtonClick = () => {
    const newItem = <ToDoItem inputText={inputText} />
    setList([...listOfItems, {item: newItem, iid: isid}])
    setIsid(isid+1)
    // inputText kannattaa tyhjentää kun se on käytetty
  }
  // jos sulla on removeItem, niin olisi loogista, että olisi myös addItem

  const removeItem = (removeThisIID: number) => {
    const updatedList = listOfItems.filter(listitem => listitem.iid != removeThisIID)
    setList(updatedList)
  }

  return (
    <div className='App'>
      <input type="text" value={inputText} onChange={updateList} />
      <button onClick={handleButtonClick}>Add</button>
      {listOfItems.map((listitem) => (
        <div key={listitem.iid}>
          {listitem.item}
          <button onClick={() => removeItem(listitem.iid)}>remove</button>
        </div>
      ))}
    </div>
  )
}

export default App