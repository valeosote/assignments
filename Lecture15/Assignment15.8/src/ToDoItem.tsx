import { useState } from "react"

interface TodoItemProps {
    inputText: string
}

function ToDoItem({ inputText }: TodoItemProps) {

    const [isDone,toggleDone] = useState(false)

    const handleButtonClick = () => {
        toggleDone(!isDone)
    }

    return(
        <div>
            <button onClick={handleButtonClick}>{isDone? 'Done:' : 'ToDo:'}</button>
            { inputText }
        </div>
    )
}

export default ToDoItem