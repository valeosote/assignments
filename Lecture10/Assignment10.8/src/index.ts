let sequence = [0,1]

const countSequence = setInterval( () => {
    console.log(sequence[0])
    sequence = [sequence[1], sequence[0]+sequence[1]]
}, 1000)

// Tässä on nyt vähän kyseenalainen syntaksi. Toimii kyllä, mutta countSequence on nyt muuttujana setIntervalin paluuarvo, missä ei oikein ole järkeä. Mielummin määrittele ensin rekursiivinen funktio ja kutsu sitä sitten erikseen

// const seq = () => {
//     console.log(sequence[0])
//     sequence = [sequence[1], sequence[0]+sequence[1]]
//     setInterval(seq, 1000)
// }

// seq()

// Et myöskään tarvitse välttämättä tuota ulkopuolista muuttujaa, vaan voit hoitaa homman funktion parametreilla. Ks. mallivastaus.


