import { Request, Response, NextFunction } from 'express'
import argon from 'argon2'

//const { USER: envuser, PASSWORD: envpassword, HASH: envhash } = process.env
const { USER: envuser, HASH: envhash } = process.env

export const validatePostRequest = (req: Request, res: Response, next: NextFunction) => {
    const { name, message, author } = req.body //need to update for Note

    if (!name || !message) {
        return res.status(400).send('Must include both name and message')
    }

    if (author === undefined) {
        req.body.author = ''
    }

    next()
}

export const validatePutRequest = (req: Request, res: Response, next: NextFunction) => {
    const { name, message, author } = req.body
    if (name === undefined && message === undefined && author === undefined) {
        return res.status(400).send('Missing mandatory parameter: "name", "message" or "author"')
    }

    if (name === '' && message === '' && author === '') {
        return res.status(400).send('All parameters cannot be empty')
    }

    const id = Number(req.params.id)
    if (Number.isNaN(id)) {
        return res.status(404).send()
    } else {
        req.body.id = id
    }

    next()
} //hups, tätä ei (vieläkään) tarvittu

export const notFound = (_req: Request, res: Response) => {
    res.status(404).send('404 - Not Found')
}

export const logger = (req: Request, _res: Response, next: NextFunction) => {
    console.log(new Date(), req.method, req.url)
    if (Object.keys(req.body).length > 0) {
        console.log(req.body)
    }
    next()
}

export const authenticate = async (req: Request, res: Response, next: NextFunction) => {

    const inp = req.headers.authorization ?? ''
    if (!inp.startsWith('Basic ')) {
        return res.status(401).send('Not authorized.')
    }
    const decodedinp = Buffer.from(inp.substring(6), 'base64').toString('utf-8')
    //console.log(decodedinp)
    const [user, password] = decodedinp.split(':')

    if (user !== envuser) {
        return res.status(401).send('Invalid username or password')
    }

    //const hash = await argon.hash(password)
    //console.log(hash)
    //console.log(envhash)
    const validPassword = await argon.verify(envhash ?? 'undefinedcreptin', password)
    //const validPassword = (password===envpassword)
    
    if (!validPassword) {
        return res.status(401).send('Invalid username or password')
    }

    next()
}