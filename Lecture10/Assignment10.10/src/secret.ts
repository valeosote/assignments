import express, { Request, Response } from 'express'
import { logger, notFound, validatePostRequest, authenticate } from './middleware'

const router = express.Router()

interface Note {
    id: number
    time: string
    name: string
    message: string
    author?: string
}

let notes: Array<Note> = []

router.post('/', authenticate, validatePostRequest, (req: Request, res: Response) => {
    const { name, message, author } = req.body
    const id = notes.reduce((acc, cur) => cur.id > acc ? cur.id : acc, 0) + 1
    const time = (new Date()).toString()
    const newNote = { id, time, name, message, author }
    notes = notes.concat(newNote)
    res.status(201).send()
})

router.get('/', authenticate, (_req: Request, res: Response) => {
    res.send(notes)     //tästä ainakin näkee mitä id nroita on haettavissa
})

router.get('/:id', authenticate, (req: Request, res: Response) => {
    const note = notes.find(notes => notes.id === Number(req.params.id))
    note === undefined
        ? res.status(404).send()
        : res.send(note)
})

router.delete('/:id', authenticate, (req: Request, res: Response) => {
    const newNotes = notes.filter(notes => notes.id !== Number(req.params.id))
    if (newNotes.length === notes.length) {
        return res.status(404).send()
    }
    notes = newNotes
    res.status(204).send()
})


export default router