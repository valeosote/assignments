# Assignment 10.7: Comparing Base Images

Write a Node.js application that prints a single line of text. Notice that you do not need a package.json file nor any dependencies for this. Create four Docker images of the application with

* node:latest base image
* node:slim base image
* node:alpine base image
* alpine:latest linux base image with Node and NPM installed with RUN apk add --update nodejs npm

All Node images of course have Node and NPM pre-installed. Alpine Linux is just a (very lightweight) linux distribution, so it does not come with Node and NPM. They need to be installed before Node programs can be executed.

Compare the sizes of the images. Why would you prefer one of these images over another?

### Käytin komentoja
```bash
docker build -t nodelatest -f Dockerfile.nodelatest .
docker run nodelatest

docker build -t nodeslim -f Dockerfile.nodeslim . 
docker run nodeslim

docker build -t nodealpine -f Dockerfile.nodealpine . 
docker run nodealpine

docker build -t alpinelatest -f Dockerfile.alpinelatest .
docker run alpinelatest
```

### Image infoa
Näin ajoin js-tiedoston neljä kertaa ja tuotettiin tällaisia imageja:

```bash
REPOSITORY      TAG       IMAGE ID       CREATED          SIZE
alpinelatest    latest    8e837f36cf00   40 seconds ago   72.6MB
nodealpine      latest    0168e002a847   2 minutes ago    141MB
nodeslim        latest    18459b6fb178   4 minutes ago    206MB
nodelatest      latest    0adecfc8b539   12 minutes ago   1.1GB
```

## Analysis and Conclusion
It seems that the instructions were given in the order that kept creating a smaller and smaller image.
If there are no other concerns than filesize, I don't see why alpinelatest wouldn't be preferred.
However, in practical use maybe we can anticipate some additional functionality that will
be added to the project, and a more versatile package might be preferred. I guess it doesn't take
too much effort to experiment with a simple project like this one, but with larger projects some
other aspects might come into play.

// Good analysis

