import 'dotenv/config'
import server from './server'

const port = process.env.PORT ?? 3006
server.listen(port, () => {
    console.log('Server listening port', port)
})
