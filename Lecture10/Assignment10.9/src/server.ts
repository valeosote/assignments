import express, { Request, Response } from 'express'
import { logger, notFound, validatePostRequest } from './middleware'

const server = express()
server.use(express.json())
server.use(logger)

server.use(express.static('public')) // sulla ei ole täällä public-kansiota

interface Note {
    id: number
    time: string
    name: string
    message: string
    author?: string
}

let notes: Array<Note> = []

server.post('/', validatePostRequest, (req: Request, res: Response) => {
    const { name, message, author } = req.body
    const id = notes.reduce((acc, cur) => cur.id > acc ? cur.id : acc, 0) + 1
    const time = (new Date()).toString() // tässä ei oo väliä, mutta .toISOString() on monesti kiva
    const newNote = { id, time, name, message, author }
    notes = notes.concat(newNote)
    res.status(201).send()
})

server.get('/', (_req: Request, res: Response) => {
    res.send(notes)     //tästä ainakin näkee mitä id nroita on haettavissa
})

server.get('/:id', (req: Request, res: Response) => {
    const note = notes.find(notes => notes.id === Number(req.params.id))
    note === undefined
        ? res.status(404).send()
        : res.send(note)
})
// jos pitää hakea koko lista jotta voi hakea yhden itemin, niin yhden itemin hakeminen on melko turhaa :)

server.delete('/:id', (req: Request, res: Response) => {
    const newNotes = notes.filter(notes => notes.id !== Number(req.params.id))
    if (newNotes.length === notes.length) {
        return res.status(404).send()
    }
    notes = newNotes
    res.status(204).send()
})

server.use(notFound)

export default server

// Selkeä API!

// Mutta täältä puuttuu tehtävä 10 kokonaan. 