/*Assignment 5.4: Methodical Recipes
Add a metod "setServings" to your recipe. The method should take one parameter: servings (number).
The method should scale the recipe so that after the method has been used,
the recipe servings have been set to the new number, and all the ingredient amounts have been scaled accordingly.*/

interface Ingredient {
      name: string,
      amount: number
}

interface Recipe {
    name: string,
    ingredients: Ingredient[],
    servings: number,
    setServings: (arg0: number) => void
}

const eggs2: Ingredient = {
    name: 'egg',
    amount: 2
}

const bacon100: Ingredient = {
    name: 'bacon',
    amount: 100
}

const breakfast: Recipe = {
    name: 'Bacon and Eggs',
    ingredients: [eggs2, bacon100],
    servings: 4,
    setServings: function(uusikoko: number) {
        this.ingredients.forEach( x => x.amount = x.amount*uusikoko/this.servings )
        this.servings = uusikoko
    }
}

console.log(breakfast)

breakfast.setServings(8)

console.log(breakfast)

breakfast.setServings(1)

console.log(breakfast)