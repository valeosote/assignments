/*Assignment 5.5: Classy Recipes
Create two classes, Ingredient and Recipe, that you can use to create recipes just as you did in the previous exercises.

Add a "toString" method to the Recipe class, that returns a string representation of the recipe.*/

class Ingredient {
    name: string
    amount: number

    constructor(name: string, amount: number){
        this.name = name
        this.amount = amount
    }

    toString(){
        return `${this.name} (${this.amount})`
    }
}

class Recipe {
    name: string
    ingredients: Array<Ingredient>
    servings: number
    //setServings: (arg0: number) => void

    constructor(name: string, ingredients: Ingredient[], servings: number){
        this.name = name
        this.ingredients = ingredients
        this.servings = servings
    }

    setServings(uusikoko: number) {
        this.ingredients.forEach( x => x.amount = x.amount * uusikoko / this.servings)
        this.servings = uusikoko
    }

    toString() {
        const ings = this.ingredients.map( x => x.toString()).join(', ')
        return `${this.name} consists of ${ings}; serving ${this.servings} in total.`
    }
}

const eggs5 = new Ingredient('egg',2)
const bacon5 = new Ingredient('bacon',100)

const breakfast = new Recipe('Bacon and Eggs',[eggs5, bacon5],4)


console.log(breakfast)
breakfast.setServings(8)
console.log(breakfast.toString())
breakfast.setServings(1)
console.log(breakfast)
console.log(breakfast.toString())