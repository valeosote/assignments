/*Assignment 5.3: Objective Recipes
Create two interfaces: "Ingredient" and "Recipe". 

Ingredients should have two properties: name (string) and amount (number), signifying the name and amount of a single ingredient
used in a recipe. 
Recipes should have three properties: name (string), ingredients (list of Ingredient objects) and servings (number). 

Create some ingredient objects and use them to create a recipe object.*/

interface Ingredient {
      name: string,
      amount: number
}

interface Recipe {
    name: string,
    ingredients: Ingredient[],
    servings: number
}

const eggs: Ingredient = {
    name: 'egg',
    amount: 2
}

const bacon: Ingredient = {
    name: 'bacon',
    amount: 100
}

const breakfast: Recipe = {
    name: 'Bacon and Eggs',
    ingredients: [eggs, bacon],
    servings: 4 // ei tuosta neljälle riitä...
}

console.log(breakfast)