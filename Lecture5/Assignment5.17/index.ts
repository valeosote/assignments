/* Extra Assignment 5.17: Calculator
Create a class Calculator. The calculator has a method calculate(operator: string, n1: number, n2: number, ... ).
The first parameter is the name of the operation, and the following parameters are an arbitrary amount of numeric parameters.
The calculate method performs the operation defined by the operator parameter to all parameters one by one,
using the result of the previous operation in the next operation.

For example, if one would run calculate('+', 1, 3, 5, 7, 11, 13), the calculator would execute a summation.
It would start by calculating the sum of the first and the second numeric parameters, 1 + 3 = 4. Then it would
add the third parameter to the result, 4 + 5 = 9. Then it would add the next parameter to that that result,
getting 9 + 7 = 16, and continuing like this to the end. Finally it would return the result of 40.

When first initiated, a Calculator object should be able to handle multiplication ("+") or substraction ("-").
It should also have a method addOperation that can be used to add more operations. You should be able to use the
addOperation to add any operation that takes two numbers as parameters and returns a new number.

For example, one should be able to add a multiplication operation, after which the user could run calculate('', 2, 4, 6)*,
getting 48 as a result. */

/* Tässä nyt kävi niin, että ymmärtääkseni tehtävänantoa paremmin kysyin tekoälyltä, että mitä se tekisi.
 Alla on suoraan sen vastaus. Siitä näin, että joo, näinhän se tietenkin menee. Kun tämä nyt oli ekstratehtävä,
 eikä mulla ollut niin kovasti aikaa, en alkanut tätä itse kirjoittamaan uusiksi. (Ajattelin kuitenkin tallentaa
 tämän vastauksen tänne.) Ehkä omaa hahmoitusta haastoi juuri toi funktioiden syöttäminen toisiin funktioihin, 
 jonka toki ymmärsin teoriassa mutta en oikein ollut itse kuitenkaan pyöritellyt käytännössä. Koitan siis sanoa,
 että itse olisi kestänyt jonkin aikaa tämän pähkäilyyn, mutta näin avustetusti tämä meni aika nopeasti. :-)
*/

class Calculator {
    operations: { [key: string]: (a: number, b: number) => number }

    constructor() {
        this.operations = {
            '+': (a, b) => a + b,
            '-': (a, b) => a - b
        }
    }

    addOperation(operator: string, operation: (a: number, b: number) => number) {
        this.operations[operator] = operation
    }

    calculate(operator: string, ...numbers: number[]) {
        if (!this.operations[operator]) { // suosi TS:ssä eksplisiittistä vertausta, eli this.operations[operator] === undefined
            throw new Error(`Operation ${operator} not supported.`)
        }

        return numbers.reduce(this.operations[operator])
    }
}

const calculator = new Calculator()

console.log(calculator.calculate('+', 1, 3, 5, 7, 11, 13))  // Outputs: 40

console.log(calculator.calculate('-', 100, 5, 2))  // Outputs: 93

calculator.addOperation('*', (a, b) => a * b)
console.log(calculator.calculate('*', 2, 4, 6))  // Outputs: 48

calculator.addOperation('/', (a, b) => a / b)
console.log(calculator.calculate('/', 100, 5, 2))  // Outputs: 10


// Tosi hyviä vastauksia, selkeää koodia kaikissa tehtävissä. 