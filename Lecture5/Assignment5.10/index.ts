/* Assignment 5.10: Point
Create class Point that has two properties: x and y (numbers).
The coordinate class should have four methods: moveNorth, moveEast, moveSouth, and moveWest.
Each method should take one parameter: distance (number). Using the method should move the
point the given distance to the corresponding direction. */

class Point {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }

    moveNorth(distance: number) {
        this.y += distance
    }
    moveEast(distance: number) {
        this.x += distance
    }
    moveSouth(distance: number) {
        this.y -= distance
    }
    moveWest(distance: number) {
        this.x -= distance
    }
}

const piste = new Point(0,0)

console.log(piste)

piste.moveNorth(10)
piste.moveWest(5)

console.log(piste)