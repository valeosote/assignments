/* Assignment 5.9: Forecast
Create forecast_data.json and then add to following data to

{
  "day": "monday",
  "temperature": 25,
  "cloudy": true,
  "sunny": false,
  "windy": false
}

Then retrieve the data and modify the temperature of that forecast. Lastly, save the change back to the file. */

import fs from 'fs'

const fdata = fs.readFileSync('forecast_data.json', 'utf8')

const ennuste = JSON.parse(fdata)

console.log(ennuste)

ennuste.temperature = 30

console.log(ennuste)

const jdata = JSON.stringify(ennuste)
// tonne kun laittaa .stringify(ennuste, null, 4) niin pysyy tiedoston muotoilut

fs.writeFileSync('forecast_data.json', jdata, 'utf8')
