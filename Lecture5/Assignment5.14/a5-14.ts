/*Assignment 5.14: Credential Generator
Create a function (or multiple functions) that generates username and salasana from given firstname and lastname.

Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case

Password: 1 random letter + first letter of first name in lowercase + last letter of last name
in uppercase + random special character + last 2 numbers from current year

Get to know to ASCII table. Random letters and special characters can be searched from ASCII table
with String.fromCharCode() method with indexes. For example String.fromCharCode(65) returns letter A.

Hints: Generate random numbers (indexes) to get one random letter and one special character.
Use range of 65 to 90 to get the (uppercase) LETTER and 33 to 47 to get the SPECIAL CHARACTER.
Notice, that these are not the only "special characters", but using this range is acceptable for this exercise.
Use build-in function to get the current year.

Links: http://www.asciitable.com/ 

generateCredentials('John', 'Doe') // [ 'B22dojo', 'KjE,22' ]*/

function generateCredentials(etunimi: string, sukunimi: string) {
    const vuosi = new Date().getFullYear().toString()

    const knimi = `B${vuosi.slice(-2)}${sukunimi.slice(0, 2).toLowerCase()}${etunimi.slice(0, 2).toLowerCase()}`

    let satunnaiskirjain = String.fromCharCode(Math.floor(Math.random() * 26) + 65)
    if (Math.random() > 0.5) {
        satunnaiskirjain = satunnaiskirjain.toLowerCase()
    }
    const erikoismerkki = String.fromCharCode(Math.floor(Math.random() * 15) + 33)
    const salasana = `${satunnaiskirjain}${etunimi[0].toLowerCase()}${sukunimi.slice(-1).toUpperCase()}${erikoismerkki}${vuosi.slice(-2)}`

    return { knimi, salasana }
}

const tyyppi = generateCredentials('Mikki', 'Hiiri')
console.log(' Mikki Hiiri ->')
console.log(`Username: ${tyyppi.knimi}`)
console.log(`Password: ${tyyppi.salasana}`)
const malli = generateCredentials('John', 'Doe')
console.log(' John Doe ->')
console.log(`Username: ${malli.knimi}`)
console.log(`Password: ${malli.salasana}`)