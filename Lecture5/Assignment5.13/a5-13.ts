/*Assignment 5.13: Likes
Create a Post class that has properties author (string), content (string), and likers (list of strings).
The author is the name of the post author and content is the content of the post.
The likers is an array of users that have liked the post.

The Post class shuld have a method like(user: string) that adds the user to the likers list.
The same user can not be added twice! It should also have a method likes that returns one of the
following strings depending on the length of the likes list:

0 likes: "No one likes this"
1 like: "[user] likes this"
2 likes: "[user1] and [user2] like this"
3 likes: "[user1], [user2] and [user3] like this
4 likes: "[user1], [user2] and 2 others like this"
5 or more likes: just as with 4 likes, but the number of "others" simply increases*/

class Post {
    author: string
    content: string
    likers: Array<string>

    constructor(author: string, content: string) {
        this.author = author
        this.content = content
        this.likers = []
    }

    like(user: string) {
        if (!this.likers.includes(user)) {
            this.likers.push(user)
        }
    }

    likes() {
        const tykkäyksiä = this.likers.length
        if (tykkäyksiä === 0) {
            return 'No one likes this'
        } if (tykkäyksiä === 1) {
            return `${this.likers[0]} likes this`
        } if (tykkäyksiä === 2) {
            return `${this.likers[0]} and ${this.likers[1]} like this`
        } if (tykkäyksiä === 3) {
            return `${this.likers[0]}, ${this.likers[1]} and ${this.likers[2]} like this`
        } if (tykkäyksiä >= 4) {
            return `${this.likers[0]}, ${this.likers[1]} and ${tykkäyksiä-2} others like this`
        }
    }
}

const pylväs = new Post('Roope','Kääk!')
console.log(pylväs.likes())
pylväs.like('Aku')
console.log(pylväs.likes())
pylväs.like('Iines')
console.log(pylväs.likes())
pylväs.like('Hannu')
console.log(pylväs.likes())
pylväs.like('Touho')
console.log(pylväs.likes())
pylväs.like('Hansu')
console.log(pylväs.likes())