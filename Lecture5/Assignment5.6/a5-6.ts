/*Assignment 5.6: Extended Recipes
Create a HotRecipe class that extends the Recipe you have already created. HotRecipe should have a heatLevel parameter in addition to the Recipe parameters
Override the existing toString method with a new one that adds a warning to the recipe if the heatLevel is over 5.

Test your code. Create a new HotRecipe object with heatLevel 3 and another with heatLevel 8. Scale both to 100 portions and print the results.*/

class Ingredientti {
    name: string
    amount: number

    constructor(name: string, amount: number){
        this.name = name
        this.amount = amount
    }

    toString(){
        return `${this.name} (${this.amount})`
    }
}

class Recipe {
    name: string
    ingredients: Array<Ingredientti>
    servings: number
    //setServings: (arg0: number) => void

    constructor(name: string, ingredients: Ingredientti[], servings: number){
        this.name = name
        this.ingredients = ingredients
        this.servings = servings
    }

    setServings(uusikoko: number) {
        this.ingredients.forEach( x => x.amount = x.amount * uusikoko / this.servings)
        this.servings = uusikoko
    }

    toString() {
        const ings = this.ingredients.map( x => x.toString()).join(', ')
        return `${this.name} consists of ${ings}; serving ${this.servings} in total.`
    }
}

class HotRecipe extends Recipe {
    heatLevel: number

    constructor(name: string, ingredients: Ingredientti[], servings: number, heatLevel: number){
        super(name, ingredients, servings)
        this.heatLevel = heatLevel
    }

    toString() {
        const ings = this.ingredients.map( x => x.toString()).join(', ')
        if (this.heatLevel > 5){
            return `WARNING! This is a hot dish! \n${this.name} consists of ${ings}; serving ${this.servings} in total with a VERY HOT heat level of ${this.heatLevel}.`
        } else {
            return `${this.name} consists of ${ings}; serving ${this.servings} in total with a heat level of ${this.heatLevel}.`
        }
    }
    setHeatLevel(uusilämpö: number){
        this.heatLevel = uusilämpö
    }
}


const eggs = new Ingredientti('egg',2)
const bacon = new Ingredientti('bacon',100)

const breakfast = new HotRecipe('Bacon and Eggs',[eggs, bacon],4,3)


console.log(breakfast)
breakfast.setServings(8)
console.log(breakfast.toString())
breakfast.setServings(1)
breakfast.setHeatLevel(10)
console.log(breakfast)
console.log(breakfast.toString())