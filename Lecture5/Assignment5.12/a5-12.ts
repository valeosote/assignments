/*Assignment 5.12: Banking
Create classes Bank and Account.

The Account class should have properties owner (string), id (string) and balance (number),
and methods deposit(amount: number) and withdraw(amount: number).
The deposit method should add the given amount to the balance.
The amount must be a positive number. The method should return true on success and false on failure.

The Bank class should have properties name (string), accounts (list of Account objects) and following methods:

openAcount(owner:string)
closeAccount(id: string)
deposit(accountId: string, amount: number)
withdraw(accountId: string, amount: number)
checkBalance(accountId: string)
transfer(fromAccountId: string, toAccountId: string, amount: number)
customers()
totalValue()

The openAccount method should create a new account with a unique id and zero balance,
add it to the list of accounts, and return the created account id.
The closeAccount method should remove the account corresponding to the given id from the list of accounts,
and return the removed Account object. It should return null on failure.

The deposit method should add the given amount to the bank account corresponding to the given account id.
It should return true on success and false on failure. The withdraw method should remove the given amount
from the account corresponding to the given account id. It should return the removed sum, or null on failure.

The checkBalance should return the balance of the account corresponding to the given id, or null on failure.
The transfer method should move money from one account to another. It should return true on success and false on failure.
Note that if either the removal or the addition fails for any reason, the other operation should also not happen.

The customers method returns the list of all account owners, and the totalValue method returns the sum of all account balances.

Test your code by creating a bank and some customers and using all the methods,
checking that the results are what they should be.*/

class Account {
    owner: string
    id: string
    balance: number

    constructor(owner: string, id: string, balance: number) {
        this.owner = owner
        this.id = id
        this.balance = balance
    }

    deposit(amount: number) {
        if (amount >= 0) {
            this.balance += amount
            return true
        } else {
            return false
        } 
    }

    withdraw(amount: number) {
        //it would make sense to also check that the amount is not greater than the balance, but this functionality was not requested
        // which was definitely a mistake in the assignment :) will be fixed
        if (amount >= 0) {
            this.balance -= amount
            return true
        } else {
            return false
        }
    } 
}

class Bank {
    name: string
    accounts: Array<Account>

    constructor(name: string, accounts: Array<Account>) {
        this.name = name
        this.accounts = accounts
    }

    /* Okei, vähän selitän tätä alla olevaa indeksöintiä. Halusin kokeilla, että jos saisin listattua lukuja mielenkiintoisemmalle
       tavalla kuin vain alkaen 1,2,3... Tämä ei ollut vaikeaa, ja luvut eivät toistu ensimmäisen miljoonan avatun tilin kanssa.
       ...Paitsi sitten houmasinkin ongelman sen kanssa, että käytin viitteenä tilien lukumäärää. Jos jokin tili suljetaan, niin
       seuraavan avatun tilin id olikin jo käytetty. Lisäsin sitten tämän for-loopin joka tarkistaa, ettei uusi tilin id ole
       käytössä sillä hetkellä. Tämä kuitenkin tarkoittaa, että poistettujen tilien id ovat vapaata riistaa uusille tileille.
       Tämän voisi kiertää käyttämällä sisäistä laskinta siitä, kuinka monta tiliä on koskaan luotu, joka myös mahdollistaisi
       for-loopin poistamisen. Lopputulos toimii, mutta ei tällä kertaa niin elegantisti kuin olin toivonut. :-)
    */
   // Tähän ehdottomasti joku generateId metodi, johon oma kommentti sen toimintalogiikasta, sitten homma on bueno. Nyt siellä on eri aiheen logiikkaa seassa, mikä on aina sekavaa lukea.
    openAccount(owner: string) {
        let idn = (430072).toString()
        for ( let i = 4; i < 1_000_004; i++) {
            if (this.accounts.findIndex(account => account.id === idn) == -1){
                break
            }
            idn = (1_000_003 - ((this.accounts.length + i) * 189_977) % 1_000_003).toString()
        }
        const tili = new Account(owner, idn, 0)
        this.accounts.push(tili)
        return idn
    }



    closeAccount(id: string) {
        const index = this.accounts.findIndex(account => account.id === id)
        if (index !== -1) {
            return this.accounts.splice(index, 1)[0]
        }
        // käytä .find, kun et tarvitse indeksiä vaan koko objektin. Lähtökohtasesti .findIndex tarvitaan tosi harvoin
        return null
    }

    deposit(id: string, amount: number) {
        const index = this.accounts.findIndex(account => account.id === id)
        if (index != -1) {
            return this.accounts[index].deposit(amount)
        }
        return false
    }

    withdraw(id: string, amount: number) {
        const index = this.accounts.findIndex(account => account.id === id)
        if (index != -1) {
            if (this.accounts[index].withdraw(amount)) {
                return amount
            }
        }
        return null
    }

    checkBalance(id: string) {
        const index = this.accounts.findIndex(account => account.id === id)
        if (index != -1) {
            return this.accounts[index].balance
        }
        return null
    }

    transfer(fromId: string, toId: string, amount: number) {
        if (amount > 0) {
            const tililtäInd = this.accounts.findIndex(account => account.id === fromId)
            const tililleInd = this.accounts.findIndex(account => account.id === toId)

            if (tililtäInd != -1 && tililleInd != -1 && (this.accounts[tililtäInd].balance >= amount)) {
                if (this.accounts[tililtäInd].withdraw(amount)) {
                    if (!this.accounts[tililleInd].deposit(amount)) {
                        this.accounts[tililtäInd].deposit(amount) //ei luultavasti pitäisi tänne edes päästä
                        return false
                    }
                    return true
                }
            }
        }
        return false
    }

    customers() {
        return this.accounts.map(account => account.owner)
    }

    totalValue() {
        return this.accounts.reduce((total, account) => total + account.balance, 0)
    } // tämä oli kiva miettiä ja sit lopulta sai sen näinkin tiiviiseen muotoon :-)
}

const pankki = new Bank('Miikan Pankki', [])
const ekatili = pankki.openAccount('Tupu')
const tokatili = pankki.openAccount('Hupu')
const kolmtili = pankki.openAccount('Lupu')

console.log(pankki.deposit(ekatili,-10)) //tulostaa onnistuiko
console.log(pankki.deposit(ekatili,100))
pankki.deposit(tokatili,1000)
pankki.deposit(kolmtili,5000)
console.log(pankki.withdraw(ekatili, -20))
console.log(pankki.withdraw(tokatili, 300)) //pitäis tulostaa 300
console.log(pankki.checkBalance(tokatili)) //pitäis tulostaa 700
console.log(pankki.transfer(kolmtili, ekatili, 3233)) //tulostaa onnistuiko
console.log(pankki.transfer(tokatili, ekatili, 9999))
console.log(pankki.checkBalance(ekatili))
console.log(pankki.customers())
console.log(pankki.totalValue())
console.log(pankki.closeAccount(kolmtili))
console.log(pankki.checkBalance(kolmtili))
const neljtili = pankki.openAccount('Mortti')
const viidtili = pankki.openAccount('Vertti')
pankki.deposit(neljtili, 100000)
pankki.deposit(viidtili, 0)
console.log(pankki.customers())
console.log(pankki.totalValue())
console.log(pankki.accounts.map(account => account.id))

// kaikki tän tehtävän findIndex metodit kannattaa olla vaan find