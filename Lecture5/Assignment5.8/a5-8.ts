/*Assignment 5.8: Read/Write File
Implement a program that can read your given file, and print out all the information in it.

Print out word for word, and every time the word you’re trying to print is “joulu”, you replace that word with “kinkku”,
and every time your printing “lapsilla” you print “poroilla”.

Finally, write the altered text to a new file.
*/

import fs from 'fs'

let teksti: string = fs.readFileSync('./joulu.txt', 'utf-8')

console.log(teksti)

let sanat = teksti.split(' ')

function suodata(sana: string){
    if (sana == 'joulu'){
        return 'kinkku'
    } else if (sana == 'lapsilla'){
        return 'poroilla'
    }
    return sana
}
// hyvä apufunktio

teksti = sanat.map( x => suodata(x) ).join(' ')
// Ehkä selkeämmin
// const uusiTeksti = teksti.split(' ').map(...).join(' ')

console.log(teksti)

fs.writeFileSync('./kinkku.txt', teksti, 'utf-8')