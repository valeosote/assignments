/*Assignment 5.11: SpacePoint
Create a class SpacePoint that extend the class from the previous assignment,
so that the new class can handle three-dimensional movement.

...extends Assignment 5.10: Point
Create class Point that has two properties: x and y (numbers).
The coordinate class should have four methods: moveNorth, moveEast, moveSouth, and moveWest.
Each method should take one parameter: distance (number). Using the method should move the
point the given distance to the corresponding direction.*/

class Point {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }

    moveNorth(distance: number) {
        this.y += distance
    }
    moveEast(distance: number) {
        this.x += distance
    }
    moveSouth(distance: number) {
        this.y -= distance
    }
    moveWest(distance: number) {
        this.x -= distance
    }
}

class SpacePoint extends Point {
    z: number

    constructor(x: number, y: number, z: number){
        super(x, y)
        this.z = z
    }

    moveUp(distance: number) {
        this.z += distance
    }
    moveDown(distance: number) {
        this.z -= distance
    }
}

const paikka = new SpacePoint(1,-1,5)

paikka.moveDown(1)
console.log(paikka)

paikka.moveNorth(10)
paikka.moveWest(5)
paikka.moveUp(4)

console.log(paikka)