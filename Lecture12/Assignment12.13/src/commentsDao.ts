import { executeQuery } from './db'

export interface Comment {
    user_id: number
    post_id: number
    content: string
    comment_date?: string
    comment_id?: number
}

export const getCommentsByUser = async (user_id: number) => {
    const query = 'SELECT * FROM comments WHERE user_id=$1'
    const parameters = [user_id]
    const result = await executeQuery(query, parameters)
    const comments: Array<Comment> = result.rows
    return comments
}

export const addComment = async (user_id: number, post_id: number, content: string) => {
    const query = 'INSERT INTO comments (user_id, post_id, content) VALUES ($1, $2, $3) RETURNING comment_id, comment_date'
    const parameters = [user_id, post_id, content]
    const result = await executeQuery(query, parameters)
    const comment_id = result.rows[0].comment_id
    const comment_date = result.rows[0].comment_date
    const comment: Comment = { user_id, post_id, content, comment_date, comment_id }
    return comment
}
