import express, { Request, Response } from 'express'
import { getCommentsByUser, addComment } from './commentsDao'

interface Comment {
    user_id: number
    post_id: number
    content: string
    comment_date: string
    comment_id: number
}

const router = express.Router()

let comments: Array<Comment> = []

function generateCommentId() {
    let n = comments.length + 1
    while( comments.some(com => com.comment_id === n) ){
        n++
    }
    return n
}

router.get('/:userID', async (req: Request, res: Response) => {
    const user_id = Number(req.params.userID)
    const userComments = await getCommentsByUser(user_id)
    userComments.length === 0
        ? res.status(404).send('No comments found for this user')
        : res.status(200).send(userComments)
})

router.post('/', async (req: Request, res: Response) => {
    const { user_id, post_id, content } = req.body

    if (!user_id || !post_id || !content) {
        return res.status(400).send('Please include proper data')
    }

    const newComment = await addComment(user_id, post_id, content)
    res.status(201).send(newComment)
})

export default router
