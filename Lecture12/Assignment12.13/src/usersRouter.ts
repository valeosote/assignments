import express, { Request, Response } from 'express'
//import argon2 from 'argon2'
//import jwt from 'jsonwebtoken'
import { getUsers, getUserWithID, addUser } from './usersDao'

const router = express.Router()

router.post('/', async (req: Request, res: Response) => {
    const { username, fullname, email } = req.body
    if (!username || !fullname || !email) {
        return res.status(400).send('Missing info')
    }
    const usersList = await getUsers()
    const existingUser = usersList.find(user => user.username === username)
    if (existingUser !== undefined) {
        return res.status(400).send('Username already taken')
    }
    const user = await addUser(username, fullname, email)
    res.status(201).send(user)
})

router.get('/', async (_req: Request, res: Response) => {
    const usersList = await getUsers()
    res.status(200).send(usersList)
})

router.get('/:id', async (req: Request, res: Response) => {
    const user_id = Number(req.params.id)
    const user = await getUserWithID(user_id)
    if (user === undefined) {
        res.status(404).send()
    }
    res.status(200).send(user)
})

export default router