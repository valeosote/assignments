import { executeQuery } from './db'

export interface Post {
        user_id: number
        title: string
        content?: string
        post_date?: string
        post_id?: number
}

export const getPosts = async () => {
    const query = 'SELECT post_id, user_id, title FROM posts'
    const result = await executeQuery(query)
    const posts: Array<Post> = result.rows
    return posts
}

export const getPostInfo = async (post_id: number) => {
    const query = `SELECT
                        posts.post_id,
                        posts.user_id AS poster_id,
                        posts.title,
                        posts.content AS post_content,
                        posts.post_date,
                        comments.user_id AS commentor_id,
                        comments.content AS comment_content,
                        comments.comment_date
                    FROM posts
                    LEFT JOIN
                        comments ON posts.post_id = comments.post_id
                    WHERE
                        posts.post_id = $1
                    ORDER BY
                        posts.post_date DESC,
                        comments.comment_date DESC NULLS LAST;`
    const parameters = [post_id]
    const result = await executeQuery(query, parameters)
    return result.rows
}

export const addPost = async (user_id: number, title: string, content: string) => {
    const query = 'INSERT INTO posts (user_id, title, content) VALUES ($1, $2, $3) RETURNING post_id, post_date'
    const parameters = [user_id, title, content]
    const result = await executeQuery(query, parameters)
    const post_id = result.rows[0].post_id
    const post_date = result.rows[0].post_date
    const post: Post = { user_id, title, content, post_date, post_id }
    return post
}