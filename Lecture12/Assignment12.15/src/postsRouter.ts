import express, { Request, Response } from 'express'
//import { validatePostRequest, validatePutRequest, authenticateUser, authenticateAdmin } from './middleware'
import { getPosts, getPostInfo, addPost, delPost } from './postsDao'

const router = express.Router()

router.get('/', async (_req: Request, res: Response) => {
    const postsList = await getPosts()
    res.status(200).send(postsList)
})

router.get('/:id', async (req: Request, res: Response) => {
    const post_id = Number(req.params.id)
    const postInfo = await getPostInfo(post_id)
    postInfo.length === 0
        ? res.status(404).send()
        : res.status(200).send(postInfo)
})

router.post('/', async (req: Request, res: Response) => {
    const { user_id, title, content } = req.body

    if (!user_id || !title || !content) {
        return res.status(400).send('Please include proper data')
    }

    const newPost = await addPost(user_id, title, content)
    res.status(201).send(newPost)
})

router.delete('/:id', async (req: Request, res: Response) => {
    const post_id = Number(req.params.id)
    await delPost(post_id)
    res.status(204).send()
})

export default router