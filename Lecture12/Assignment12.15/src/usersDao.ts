import { executeQuery } from './db'

export interface User {
    username: string
    fullname?: string
    email?: string
    user_id?: number
}

export const getUsers = async () => {
    const query = 'SELECT user_id, username FROM users'
    const result = await executeQuery(query)
    const users: Array<User> = result.rows
    return users
}

export const getUserWithID = async (user_id: number) => {
    const query = 'SELECT * FROM users WHERE user_id=$1'
    const parameters = [user_id]
    const result = await executeQuery(query, parameters)
    if (result.rows.length === 0) {
        return undefined
    }
    const user: User = result.rows[0]
    //console.log(result)
    return user
}

export const addUser = async (username: string, fullname: string, email: string) => {
    const query = 'INSERT INTO users (username, fullname, email) VALUES ($1, $2, $3) RETURNING user_id'
    const parameters = [username, fullname, email]
    const result = await executeQuery(query, parameters)
    const user_id = result.rows[0].user_id
    const user: User = { username, fullname, email, user_id }
    return user
}

export const delUser = async (user_id: number) => {
    const query = 'DELETE FROM users WHERE user_id=$1'
    const parameters = [user_id]
    await executeQuery(query, parameters)
}