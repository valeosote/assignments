//import 'dotenv/config'
import express from 'express'
import helmet from 'helmet'
import usersRouter from './usersRouter'
import postsRouter from './postsRouter'
import commentsRouter from './commentsRouter'
import { logger, notFound } from './middleware'
//import { queryAllTables } from './db'

//const letstakealook = queryAllTables()
//console.log(letstakealook) //tais ollakin async

const server = express()
server.use(helmet())

server.use(express.json())
server.use(logger)

server.use('/users', usersRouter)
server.use('/posts', postsRouter)
server.use('/comments', commentsRouter)

server.use(notFound)

server.listen(3000, () => {
    console.log('Server listening to port', 3000)
})