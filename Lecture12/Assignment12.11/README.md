# Assignment 12.11: Create Content
Create five users. Create five posts. Create five comments.

```INSERT INTO users (username, fullname, email)
VALUES
    ('ross', 'Ross Geller', 'ross.geller@gmail.com'),
    ('rachel', 'Rachel Green', 'rachel.green@apple.com'),
    ('joey', 'Joey Tribbiani', 'joey.tribbiani@amazon.com'),
    ('phoebe', 'Phoebe Buffay', 'phoebe.buffay@microsoft.com'),
    ('monica', 'Monica Geller', 'monica.geller@google.com');
	
INSERT INTO posts (user_id, title, content, post_date)
VALUES
    (1, 'need help with couch', 'I need a few extra hands to come help me move some furniture.', CURRENT_TIMESTAMP),
    (2, 'cant find the keys', 'Monica, I know you took them. Call me!', CURRENT_TIMESTAMP),
    (3, 'is dinner ready yet?', 'Whats not to like? Custard, good. Jam, good. Meat, good!', CURRENT_TIMESTAMP),
    (4, 'new songs on the way', 'I m stuck on a couple song titles. You can give me some suggestions, but I will probably use my own anyway.', CURRENT_TIMESTAMP),
    (5, 'does anyone need a break?', 'I have been working so much but I don not have time for breaks. Could someone take a break on my behalf?', CURRENT_TIMESTAMP);

INSERT INTO comments (user_id, post_id, content, comment_date)
VALUES
    (2, 1, 'I can come. Chandler said he is free as well', CURRENT_TIMESTAMP),
    (5, 2, 'Sorry, it must have been a mistake. On my way there now.', CURRENT_TIMESTAMP),
    (1, 4, 'Smelly rocks.', CURRENT_TIMESTAMP),
    (3, 4, 'Sassy Jingle', CURRENT_TIMESTAMP),
    (1, 5, 'Ask Ross, he is an expert.', CURRENT_TIMESTAMP);


SELECT
    u.username, c.content AS comment_content, p.title, p.content AS post_content
FROM
    comments c
JOIN
	posts p ON c.post_id = p.post_id
JOIN
    users u ON u.user_id = c.user_id;```
	
![Results of the above query.](/MT_bet_20240130_db_example_data.png)