# Assignment 12.12: Modify Content
Update one email address. Delete one user.

```UPDATE users
SET email = 'joey@nbc.com'
WHERE username = 'joey';

INSERT INTO users (username, fullname, email)
VALUES ('chandler', 'Chandler Bing', 'chandler@bing.com');

DELETE FROM users
WHERE user_id = 6;

SELECT * from users;```