// Assignment 4.5: Callback
/*Create a function that calculates the sum of all the numbers that are 
-Smaller than a billion (1_000_000_000)
-Divisible by 3, 5 and 7

The function should take one parameter, a callback function that is called when the computation is done.
The callback function should be called with the result as an argument.

Test your code by giving the function a simple arrow function that logs the result.*/

const laskeeKivasti = (mihinasti=1_000_000_000-1) => {
    let summa = 0
    while (mihinasti > 0) {
        if (mihinasti % (3*5*7) === 0) {
            summa += mihinasti
        }
        mihinasti--
    }
    return summa
}

const callMeMaybe = (callback: (result:number) => void) => {
    const result = laskeeKivasti()
    callback(result)
}

callMeMaybe((tulos) => console.log(tulos))

const x = Math.floor((1000000000-1) / (3*5*7))
const y = (3*5*7) * x * (x+1) / 2
console.log( y )