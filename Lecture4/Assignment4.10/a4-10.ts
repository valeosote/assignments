// Assignment 4.10: find
// Find the first name in the list that is three letters long and ends in letter t.

const names = [
    'Murphy',
    'Hayden',
    'Parker',
    'Arden',
    'George',
    'Andie',
    'Ray',
    'Storm',
    'Tyler',
    'Pat',
    'Keegan',
    'Carroll'
]

const foundIt = names.find( nimi => nimi.length == 3 && nimi.charAt(2) === 't')

console.log(foundIt)