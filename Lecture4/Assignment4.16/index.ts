/* Assignment 4.16: Range Within
Write a program that takes in any two numbers from the command line, start and end.
The program creates and prints an array filled with numbers from start to end.

Examples:
node ./dist/index.js 1 5 -> [1, 2, 3, 4, 5]
node ./dist/index.js -5 -1 -> [-5, -4, -3, -2, -1]
node ./dist/index.js 9 5 -> [9, 8, 7, 6, 5]

Note the order of the values. When start is smaller than end, the order is ascending
and when start is greater than end, order is descending.

On this assignment you can't use "npm start" script directly, since Node parses negative numbers as flags.
You can create a fix if you want, but the easiest way to test the script is to run the compiled JS file
directly as in the example. */

const alku = Number(process.argv[2])
const loppu = Number(process.argv[3])

const suunta = alku>loppu ? -1 : 1
const tulos: number[] = []

//tulos.push(alku)
//tulos.push(loppu)

for ( let i = alku; i != loppu + suunta; i += suunta){
    tulos.push(i)
}

//vähän riskaabeli looppi, jos käyttäjä antaa luvut joiden erotus ei ole kokonaisluku

console.log(tulos)