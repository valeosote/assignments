// 4.3 Function Flavours
/*
Write a function that takes either 2 or 3 parameters, then calculates the sum of these parameters and returns the result.

Do this three times: 
Using a named function
Using an anonymous function
Using an arrow function*/

function kaksitaikolme(a: number, b:number, c: number = 0): number{
    return a+b+c
}

const kaksikolme = function (a:number, b:number, c:number = 0) {
    return a+b+c
}

const kaksikolmenuoli = (a:number, b:number, c:number = 0) => a+b+c

const sis1 = Number(process.argv[2])
const sis2 = Number(process.argv[3])
const sis3 = process.argv[4]!==undefined ? Number(process.argv[4]) : 0

//en nyt ollut ihan vakuuttunut, että näin toimi parhaiten mutta tämän sain aikaan annetussa ajassa

console.log(kaksitaikolme(sis1,sis2,sis3))

console.log(kaksikolme(sis1,sis2,sis3))

console.log(kaksikolmenuoli(sis1,sis2,sis3))
