/* Assignment 4.20: Fibonacci Sequence
The Fibonacci sequence is a sequence of numbers where each number is the sum of the two preceding ones, starting from 0 and 1.
Create a function that produces a Fibonacci sequence of length n.

For example, if n = 8, the program should produce a sequence [0,1,1,2,3,5,8,13]. */

const luku = Number(process.argv[2])

const fibba = (n: number): number[] => {
    if (n <= 1) {
        return [0]
    } else if (n === 2) {
        return fibba(n-1).concat([1])
    } else {
        return fibba(n-1).concat(fibba(n-1)[fibba(n-1).length-1]+fibba(n-2)[fibba(n-2).length-1])
    }
}
//tehokkaimpia rekursioita joita on kukaan ikinä nähnytkään (itse sain koneen melkein jumiin luvulla 20)

console.log(fibba(luku))