/* Assignment 4.21: Prime Numbers
A prime number (or a prime) is a natural number (aka positive integer) greater than 1
that is not a product of two smaller natural numbers. For example, 4 is not a prime
because it is divisible by 2. On the other hand, 7 is a prime: it is only divisible by 7 and 1.

Create a function that checks whether a given number is a prime number. */

const luku = Number(process.argv[2])

function alkulukuko(n: number) : boolean {
    if (luku > 30) {
        if (luku % 2 === 0) {
            return false
        }else{
            for (let i = 3; i * i < luku; i += 2) {
                if (luku % i === 0) {
                    return false
                }
            }
        }
    } else if (![2,3,5,7,11,13,17,19,23,29].includes(n)) {
        return false
    }
    return true
}

function palautetekstinä(n: number) : string {
    if (alkulukuko(n)) {
        return `Luku ${n} on alkuluku.`
    } else {
        return `Luku ${n} ei ole alkuluku.`
    }
}

console.log(palautetekstinä(luku))