/* Assignment 4.17: Reversed Words
Create a programs that reverses each word in a string.

node .\reversed_words.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes
*/

const sisään = process.argv[2]

const ulos = sisään.split(' ').map( sana => sana.split('').reverse().join('')).join(' ')

console.log(ulos)