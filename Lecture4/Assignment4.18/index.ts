/* Assignment 4.18: Palindrome
Check if given string is a palindrome.

Examples:
npm start saippuakivikauppias -> Yes, 'saippuakivikauppias' is a palindrome
npm start saippuakäpykauppias -> No, 'saippuakäpykauppias' is not a palindrome
*/

const annettuna = process.argv[2]

function onkoPalindroomi(s: string): boolean {
    return s === s.split('').reverse().join('')
}

if (onkoPalindroomi(annettuna)) {
    console.log(`Yes, '${annettuna}' is a palindrome`)
} else {
    console.log(`No, '${annettuna}' is not a palindrome`)
}
