// 4.1
// Write a function that takes one string parameter. 
// The function should first convert the first letter of each word to upper case, and then print the string.

function convertAndPrintFirstLetterToUpperCaseForEachWord(inp: string) {
    if (typeof inp === 'undefined' || inp.length === 0) {
        console.log(inp)
    } else {
        let pointer = 0
        let pointsto = inp.indexOf(' ')
        let outp = ''
        while (pointer < inp.length) {
            if (pointsto === -1) {
                pointsto = inp.length
            }
            outp += inp[pointer].toUpperCase() + inp.substring(pointer + 1, pointsto) + ' '
            pointer = pointsto + 1
            pointsto = inp.indexOf(' ', pointer)
        }
        console.log(outp.trim())
    }
}

convertAndPrintFirstLetterToUpperCaseForEachWord(process.argv[2])
