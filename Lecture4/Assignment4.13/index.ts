/*Assignment 4.13: Count Sheep
Create a program that takes in a number from the command line,
for example "npm start 3" and prints a string "1 sheep...2 sheep...3 sheep..." */

const kertaa = Number(process.argv[2])

let tulos = ''

for(let i=1 ; i<=kertaa ; i++){
    tulos += i + ' sheep...'
}

console.log(tulos)
