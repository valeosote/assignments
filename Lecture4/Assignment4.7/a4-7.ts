// Assignment 4.7: forEach
// Write a program that goes through a list of names, capitalizes each name, and the prints the name to the console.

const names = [
    'rauni',
    'matias',
    'Kimmo',
    'Heimo',
    'isko',
    'Sulevi',
    'Mikko',
    'daavid',
    'otso',
    'herkko'
]

names.forEach(nimi => console.log(nimi[0].toUpperCase() + nimi.substring(1)))