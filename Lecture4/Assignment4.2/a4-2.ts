// 4.2
// Write a function that generates a random number within range, rounded to the nearest integer.
// The Function takes in two numbers, min and max.

function satunnaisluku(a:number, b:number): number{
    if (a>b){
        [a,b] = [b,a]
    }
    return Math.floor(Math.random() * (b - a) + a)
}

console.log(satunnaisluku(10,20))