// Assignment 4.11: reduce
// Create a function that takes two parameters: a list of strings and a separator string.
// The function should join all the strings in the array, with the separator string between each element, and then return the result.

// You must use reduce. You can not use .join

let munlista = ['eka','toka','kolmas','neljäs','jne.']

const listanLyhentäjä = (listattu: string[], väliaine: string) => {
    const reducedMunLista = listattu.reduce( (acc,cur) => {
        if(acc===''){
            return cur
        }else{
            return acc+väliaine+cur
        }
    }, "")
    return reducedMunLista
}

console.log(listanLyhentäjä(munlista,', '))
