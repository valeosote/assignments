// Assignment 4.8: map
//Write a function that takes a list of strings and returns a list, where those strings are reversed.

//With the example input the result should be   ['ytic', 'etubirtsid', …, ]

const words = [
    'city',
    'distribute',
    'battlefield',
    'relationship',
    'spread',
    'orchestra',
    'directory',
    'copy',
    'raise',
    'ice'
]

const tuulee =  words.map(sana => sana.split('').reverse().join(''))

tuulee.forEach(tuuli => console.log(tuuli))