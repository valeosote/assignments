/* Assignment 4.15: Ordinal Numbers
You have two arrays:

const competitors = ['Julia', 'Mark', 'Spencer', 'Ann' , 'John', 'Joe']
const ordinals = ['st', 'nd', 'rd', 'th']

Create program that outputs competitors placements with following way:
['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor was Spencer',
'4th competitor was Ann', '5th competitor was John', '6th competitor was Joe'] */

const competitors = ['Julia', 'Mark', 'Spencer', 'Ann' , 'John', 'Joe']
const ordinals = ['st', 'nd', 'rd', 'th']

const numerot: number[] = []

for (let i = 1; i <= competitors.length; i++){
    numerot.push(i)
}

function lisääPääte( n: number ): string{
    if (n <= 3) {
        return n+ordinals[n-1]
    } else {
        return n+ordinals[3]
    }
}

const tulos = numerot.map( num => lisääPääte(num) + ' competitor was ' + competitors[num-1])

console.log(tulos)