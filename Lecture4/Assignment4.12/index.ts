/* Assignment 4.12: Array Manipulation

const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]

From the elements of this array,

1. Create a new array with only the numbers that are divisible by three.
2. Create a new array from original array (arr), where each number is multiplied by 2
3. Sum all of the values in the array using the array method reduce

After each step, console.log the result. */

const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]

const arr1 = arr.filter( x => x % 3 === 0)
console.log(arr1)

const arr2 = arr.map( x => x * 2)
console.log(arr2)

const arr3 = arr.reduce( (acc,cur) => {
    return acc+cur
}, 0)
console.log(arr3)
