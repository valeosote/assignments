import express, { Request, Responsev} from 'express'
import jwt from 'jsonwebtoken'
import 'dotenv/config'
import argon2 from 'argon2'

const router = express.Router()

interface CustomRequest extends Request {
    user?: string
}

interface User {
    username: string,
    password?: string,
    hash: string
}

let users: Array<User> = []

router.get('/', (_req: CustomRequest, res: Response) => {
    res.status(201).send()
})

router.post('/register', (req: CustomRequest, res: Response) => {
    const {username, password} = req.body
    //const user: User = {username, password}
    if (!username || !password) {
        return res.status(400).send('Invalid user information.')
    }

    let hash: string = ''
    argon2.hash(password)
        .then(result => {
            hash = result
            //console.log(hash)
            //users = users.concat({username, password, hash})
            users = users.concat({username, password, hash})
            console.log(users)

            const payload = {username: username}
            const secret98 = process.env.SECRET ?? ''
            const options = {expiresIn: '15m'}
            
            const token = jwt.sign(payload, secret98, options)
            console.log(token)

            res.status(200).send(token)
        })
})

router.post('/login', async (req: CustomRequest, res: Response) => {
    const {username, password} = req.body

    const user = users.find(u => u.username === username)

    if (!user) {
        return res.status(401).send('Unauthorized.')
    } else {
        const passwordMatch = await argon2.verify(user.hash, password)

        if (!passwordMatch) {
            return res.status(401).send('Unauthorized.')
        } else {
            const payload = {username: username}
            const secret98 = process.env.SECRET ?? ''
            const options = {expiresIn: '15m'}
            
            const token = jwt.sign(payload, secret98, options)
            console.log(token)

            return res.status(200).send(token)
        }
    }
})

router.post('/admin', async (req: Request, res: Response) => {
    const {username, password} = req.body

    const adminName = process.env.ADMIN ?? 'x'
    const adminHash = process.env.ADMINPASSWORD ?? 'y'

    //console.log(adminName, adminHash)

    const passwordMatch = await argon2.verify(adminHash, password)

    if (adminName !== username || !passwordMatch) {
        return res.status(401).send('Unauthorized.')
    }
    
    res.status(201).send()
})

/*router.get('/protected', authenticate, (req, res) => {
    res.send(`${req.user.username} accessed protected route`)
})*/

export default router