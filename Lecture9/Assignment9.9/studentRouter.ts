import express, { Request, Response } from 'express'
import { authenticate } from './middleware'

const router = express.Router()

interface Student {
    id: number,
    name: string,
    email: string
}

let students: Array<Student> = []

router.get('/', authenticate, (_req: Request, res: Response) => {
    res.send(students.map(student => student.id))
})

router.get('/:id', authenticate, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find(student => student.id === id)
    if (student === undefined) {
        res.status(404).send()
    }
    res.send(student)
})

router.post('/', authenticate, (req: Request, res: Response) => {
    const { id, name, email } = req.body
    const idNumber = Number(id)
    if (!idNumber || !name || !email) {
        return res.status(400).send('Invalid student information')
    }

    students = students.concat({ id: idNumber, name, email })
    console.log(students)
    res.status(201).send()
})

router.put('/:id', authenticate, (req: Request, res: Response) => {
    const { name, email } = req.body
    if (!name && !email) {
        return res.status(400).send('Invalid student information')
    }

    const id = Number(req.params.id)
    const student = students.find(student => student.id === id)
    if (student === undefined) {
        return res.status(404).send()
    }

    const updatedStudent: Student = {
        id: student.id,
        name: name ?? student.name,
        email: email ?? student.email
    }

    students = students.map(student => student.id === id ? updatedStudent : student)
    res.status(204).send()
})

router.delete('/:id', authenticate, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const newStudents = students.filter(student => student.id !== id)

    if (newStudents.length === students.length) {
        return res.status(404).send()
    }
    students = newStudents
    res.status(204).send()
})

export default router