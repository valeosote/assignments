import express from 'express'
import 'dotenv/config'
import { logger, notFound } from './middleware'
import studentRouter from './studentRouter'
import userRouter from './userRouter'

const server = express()

server.use(express.json())

server.use(express.static('public'))

server.use(logger)

server.use('/students', studentRouter)

server.use('/users', userRouter)

server.use(notFound)

export default server