import 'dotenv/config'
import jwt from 'jsonwebtoken'

const payload = { username: 'salainenSpider-Man' }
const secret = process.env.SECRET ?? ''
const options = { expiresIn: '15min'}

const token = jwt.sign(payload, secret, options)
console.log(token)

//const myToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InNhbGFpbmVuU3BpZGVyLU1hbiIsImlhdCI6MTcwNTMyMzIyNCwiZXhwIjoxNzA1MzI0MTI0fQ.wh0JKMvoIhG7TdvIaTuOHmqxbgfhP2puGBy0pO69lik'

const myToken ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InNhbGFpbmVuU3BpZGVyLU1hbiIsImlhdCI6MTcwNTMyMzIyNCwiZXhwIjoxNzA1MzM0MTQ0fQ.E8osjbU9ghgeoD1P2MRMISKUtaq9m0xbb9WAxtxyoPU'

try {
    const decodedToken = jwt.verify(myToken,'mullaonkinkaksitoistahiusta')
    console.log(decodedToken)
} catch (error) {
    console.log('That probably did not quite go to plan.')
}

// Jess, toimi!