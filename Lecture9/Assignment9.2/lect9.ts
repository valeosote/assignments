import express from 'express'
import { logger, notFound } from './middleware'
import studentRouter from './studentRouter'

const server = express()

server.use(express.json())

server.use(express.static('public'))

server.use(logger)

//server.use('/student', studentRouter)
server.use('/students', studentRouter)

server.use(notFound)

const port = 3000
server.listen(port, () => {
    console.log('Server listening port', port)
})
