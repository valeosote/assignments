import express, { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import argon2 from 'argon2'
import 'dotenv/config'

const router = express.Router()

interface User {
    username: string,
    hash: string
}

const admin = process.env.ADMIN ?? 'admin' //toistaiseksi tuetaan vain yhtä adminia
const adminhash = process.env.ADMINPASSWORDHASH ?? ''
let users: Array<User> = [{username: admin, hash: adminhash}]

router.post('/register', async (req: Request, res: Response) => {
    const {username, password} = req.body

    if (!username || !password) {
        return res.status(400).send('Invalid user information.')
    }

    const user = users.find(u => u.username === username)

    if (user) {
        return res.status(401).send('This username is already in use.')
    }

    const hash: string = (await argon2.hash(password)).toString()
 
    users = users.concat({username, hash})
    console.log(users)

    const payload = {username: username}
    const secret = process.env.SECRET ?? 'mullaonkinkaksitoistahiusta'
    const options = {expiresIn: '15m'}
            
    const token = jwt.sign(payload, secret, options)
    console.log(token)

    res.status(200).send(token)
})

router.post('/login', async (req: Request, res: Response) => {
    const {username, password} = req.body

    const user = users.find(u => u.username === username)

    if (!user) {
        return res.status(401).send('Unauthorized.')
    }
    
    const passwordMatch = await argon2.verify(user.hash, password)

    if (!passwordMatch) {
        return res.status(401).send('Unauthorized.')
    }

    const secret = process.env.SECRET ?? 'mullaonkinkaksitoistahiusta'
    const options = {expiresIn: '15m'}
    let payload: {username: string; isAdmin?: boolean}

    if (username === admin){
        payload = {username: username, isAdmin: true}
    } else {
        payload = {username: username}
    }

    const token = jwt.sign(payload, secret, options)
    console.log(token)

    return res.status(200).send(token)
})

export default router