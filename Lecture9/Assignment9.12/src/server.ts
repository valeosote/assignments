import express from 'express'
import helmet from 'helmet'
import { logger, notFound } from './middleware'
import booksRouter from './booksRouter'
import usersRouter from './usersRouter'

const server = express()
server.use(helmet())
server.use(express.json())
server.use(logger)

server.use(express.static('public'))

server.use('/api/v1/books', booksRouter)
server.use('/api/v1/users', usersRouter)

server.use(notFound)

export default server