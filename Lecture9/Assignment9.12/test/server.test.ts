import request from 'supertest'
import server from '../src/server'
import 'dotenv/config'


describe('Server', () => {
    it('Returns 400 on invalid user information', async () => {
        const response = await request(server)
            .post('/api/v1/users/register')
        expect(response.statusCode).toBe(400)
    })
    it('Registers a new user and returns a token', async () => {
        const response = await request(server)
            .post('/api/v1/users/register')
            .send({ username: 'Liisa', password: 'ihmemaassa' })
  
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })
    it('Attempts to register an existing user', async () => {
        const response = await request(server)
            .post('/api/v1/users/register')
            .send({ username: 'Liisa', password: 'ihmemaassa' })
  
        expect(response.status).toBe(401)
    })
  
    it('Login and returns a token', async () => {
        const response = await request(server)
            .post('/api/v1/users/login')
            .send({ username: 'Liisa', password: 'ihmemaassa' })
  
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
    })
    it('Login for admin and returns a token', async () => {
        const response = await request(server)
            .post('/api/v1/users/login')
            .send({ username: 'admin', password: 'password' })
  
        expect(response.status).toBe(200)
        expect(response.body).toBeDefined()
        //tämä nyt ei tarkista, että tuliko tokenissa sitä kaivattua isAdmin arvoa
        // tää on ihan hyvä, jos haluttais oikeesti hyvä test suite, niin pitäs konfiguroida aika monta muutakin asiaa tässä välissä, pääasia että saadaan testit toimimaan
    })
  
    it('Returns 401 on login with incorrect username', async () => {
        const response = await request(server)
            .post('/api/v1/users/login')
            .send({ username: 'Laura', password: 'ihmekuussa' })
  
        expect(response.status).toBe(401)
    })
    it('Returns 401 on login with incorrect password', async () => {
        const response = await request(server)
            .post('/api/v1/users/login')
            .send({ username: 'Liisa', password: 'ihmekuussa' })
  
        expect(response.status).toBe(401)
    })

    const userToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkxpaXNhIiwiaWF0IjoxNzA1NjQ3OTM3LCJleHAiOjE3MDU2NDg4Mzd9.R8FsUjRRVnaG2SYHlx8cDAmn8iMyUTXQCRLkziaMmo8'
    const adminToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNzA1NjQ4MzM5LCJleHAiOjE3MDU2NDkyMzl9.i24vmDU91h6qQCH28XPOfAskQSG0zGee56crjIhVb3Y'

    it('Returns all books for logged in user', async () => {
        const response = await request(server)
            .get('/api/v1/books')
            .set('Authorization', `Bearer ${userToken}`)
    
        expect(response.status).toBe(200)
        expect(response.body).toHaveLength(4)
    })
    it('Returns a book according to id', async () => {
        const response = await request(server)
            .get('/api/v1/books/1')
            .set('Authorization', `Bearer ${userToken}`)
    
        expect(response.status).toBe(200)
        expect(response.body.id).toBe(1)
    })
    it('Returns 404 for an incorrect book id', async () => {
        const response = await request(server)
            .get('/api/v1/books/999')
            .set('Authorization', `Bearer ${userToken}`)
    
        expect(response.status).toBe(404)
    })
    it('Creates a new book', async () => {
        const response = await request(server)
            .post('/api/v1/books')
            .set('Authorization', `Bearer ${adminToken}`)
            .send({name: '1984', author: 'George Orwell', read: true})

        expect(response.status).toBe(201)
    })
    it('Returns all books for admin after adding a new book', async () => {
        const response = await request(server)
            .get('/api/v1/books')
            .set('Authorization', `Bearer ${adminToken}`)
    
        expect(response.status).toBe(200)
        expect(response.body).toHaveLength(5)
    })
    it('Returns 403 when non-admin attempts to create a new book', async () => {
        const response = await request(server)
            .post('/api/v1/books')
            .set('Authorization', `Bearer ${userToken}`)
            .send({name: 'How to Solve It', author: 'George Polya', read: true})
    
        expect(response.status).toBe(403)
    })
    it('Updates an existing book', async () => {
        const response = await request(server)
            .put('/api/v1/books/1')
            .set('Authorization', `Bearer ${adminToken}`)
            .send({name: 'Taru Sormusten Herrasta', author: 'J.R.R. Tolkien', read: true})
    
        expect(response.status).toBe(204)
    })
    it('Returns 404 when updating a book using an incorrect id', async () => {
        const response = await request(server)
            .put('/api/v1/books/999')
            .set('Authorization', `Bearer ${adminToken}`)
            .send({name: 'Hämähäkkimies: Sininen', author: 'Jeph Loeb', read: true})
    
        expect(response.status).toBe(404)
    })
    it('Returns 403 when non-admin attempts to update a book', async () => {
        const response = await request(server)
            .put('/api/v1/books/1')
            .set('Authorization', `Bearer ${userToken}`)
            .send({name: 'The Lord of the Rings', author: 'John Ronald Reuel Tolkien', read: true})
    
        expect(response.status).toBe(403)
    })
    it('Returns 403 when non-admin attempts to delete a book', async () => {
        const response = await request(server)
            .delete('/api/v1/books/2')
            .set('Authorization', `Bearer ${userToken}`)
    
        expect(response.status).toBe(403)
    })
    it('Returns 404 when deleting a book using an incorrect id', async () => {
        const response = await request(server)
            .delete('/api/v1/books/99')
            .set('Authorization', `Bearer ${adminToken}`)
    
        expect(response.status).toBe(404)
    })
    it('Deletes an existing book given an id', async () => {
        const response = await request(server)
            .delete('/api/v1/books/3')
            .set('Authorization', `Bearer ${adminToken}`)
    
        expect(response.status).toBe(204)
    })
    it('Returns all books after having deleted just one', async () => {
        const response = await request(server)
            .get('/api/v1/books')
            .set('Authorization', `Bearer ${userToken}`)
    
        expect(response.status).toBe(200)
        expect(response.body).toHaveLength(4)
    })
})

// Kun yritän ajaa testit, vain 6 menee läpi ja 14 feilaa :(
// Kaikki on nyt yhden describe-blokin sisällä. Käytä describe-blokkeja jaottelemaan ja organisoimaan koodia.
// Huomaa, että tehtävänannossa pyydettiin testit ainoastana users routerille