import { Request, Response, NextFunction } from 'express'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

export const logger = (req: Request, _res: Response, next: NextFunction) => {
    console.log(new Date(), req.method, req.url)
    if (Object.keys(req.body).length > 0) {
        console.log(req.body)
    }
    next()
}

export const notFound = (_req: Request, res: Response) => {
    res.status(404).send('404 - Page Not Found')
}

interface CustomRequest extends Request {
    user?: string
}

export const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
    const auth = req.get('Authorization')
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('Invalid token.')
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET ?? ''
    try {
        const decodedToken = jwt.verify(token, secret)
        req.user = decodedToken as string //en tiedä onko tämä paras ratkaisu, mutta pääsin eroon virheistä
        next()
    } catch (error) {
        return res.status(401).send('Invalid token')
    }
}

