import express, { Request, Response } from 'express'
import argon2 from 'argon2'

const router = express.Router()

interface User {
    username: string,
    password?: string,
    hash?: string
}

let users: Array<User> = []

router.get('/', (_req: Request, res: Response) => {
    res.status(201).send()
})

router.post('/register', (req: Request, res: Response) => {
    const {username, password} = req.body
    //const user: User = {username, password}
    if (!username || !password) {
        return res.status(400).send('Invalid user information.')
    }

    let hash: string = ''
    argon2.hash(password)
        .then(result => {
            hash = result
            //console.log(hash)
            //users = users.concat({username, password, hash})
            users = users.concat({username, password, hash})
            console.log(users)
            res.status(201).send()
        })
})


export default router