import { Request, Response, NextFunction } from 'express'

export const logger = (req: Request, _res: Response, next: NextFunction) => {
    console.log(new Date(), req.method, req.url)
    if (Object.keys(req.body).length > 0) {
        console.log(req.body)
    }
    next()
}

export const notFound = (_req: Request, res: Response) => {
    res.status(404).send('404 - Page Not Found')
}