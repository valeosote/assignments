/*Assignment 8.6: 404 Not Found
Add a middleware function that sends a response with status code 404 and an appropriate error message,
if user tries to use an endpoint that does not exist.

If you have not already done so, move all your middleware to a separate file, to keep your program clean and readable.*/

import express, {Request, Response, NextFunction} from 'express'

import {middleware, unknownEndpoint} from './middlewares'

const server = express()

const students: Array<string> = []

server.use(middleware)

server.use(unknownEndpoint)

server.get('/students', (_req: Request, res: Response) => {
    
    res.send({students})
})

server.listen(7007)