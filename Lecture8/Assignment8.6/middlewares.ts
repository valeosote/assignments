/* Assignment 8.6: 404 Not Found
Add a middleware function that sends a response with status code 404 and an
appropriate error message, if user tries to use an endpoint that does not exist.

If you have not already done so, move all your middleware to a separate file,
to keep your program clean and readable.
*/
import express, { Request, Response, NextFunction} from 'express'

export const middleware = (req: Request, res: Response, next: NextFunction) => {
    
    const currentTime = new Date().toISOString()
    console.log(`${currentTime} ; ${req.method} ; ${req.url}`)
    next()
}

/*
export const errorHandler = (error, req: Request, res: Response, next: NextFunction) => {
    console.log(error.message)
    if (error.name === 'SomeUserError') {
        res.status(400).send(error.message)
    }
    next(error)
} */

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Ei löydy oppilaita täältä.'})
}