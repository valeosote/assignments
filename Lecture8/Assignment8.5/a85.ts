/*Assignment 8.5: Logger Middleware
 Create a new project called Student Registry. We will be developing this program in stages during this lecture.
 For now it should have a single GET endpoint /students that returns an empty list.

 Create a logger middleware function that is used in all the project's endpoints. The middleware should log 
 
the time the request was made
the method of the request
the url of the endpoint */

import express, { Request, Response, NextFunction} from 'express'

const server = express()

const students: Array<string> = []

const middleware = (req: Request, res: Response, next: NextFunction) => {
    
    const currentTime = new Date().toISOString()
    console.log(`${currentTime} ; ${req.method} ; ${req.url}`)
    next()
}

server.use(middleware)

server.get('/students', (_req: Request, res: Response) => {
    
    res.send({students})
})

server.listen(7007)