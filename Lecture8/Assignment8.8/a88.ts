/*Assignment 8.8: POST Requests
Add two more endpoints to your app:

POST /student should expect the request body to include student id, name and email.
If some of these parameters are missing, the endpoint should return a response with
status 400 and an error message.
The endpoint should store the student information in an array called students.
The endpoint should return an empty response with status code 201.

GET /student/:id should return the information of one single student,
identified by the id request parameter. The response should be in JSON format.
If there is no such student, the endpoint should return 404.

Modify the GET /students endpoint to return the list of all student ids, without names or emails.*/

import express, {Request, Response, NextFunction} from 'express'

import {logger, unknownEndpoint} from './middlewares'

const server = express()
server.use(express.json())

interface Body {
    id: string
    name: string
    email: string
}

const students: Array<Body> = []

server.use(logger)

const validate = (req: Request, res: Response, next: NextFunction ) => {
    const {id: id, name, email} = req.body
    if (typeof(id) !== 'string' || typeof(name) !== 'string' || typeof(email) !== 'string') {
        return res.status(400).send('Nyt puuttui jotain tai oli vääränlaista.')
    }
    next()
}

server.post('/student', validate, (req: Request, res: Response) => {
    const body: Body = req.body
    //console.log(body)
    students.push(body)
    res.sendStatus(201)
})

server.get('/student/:id', (req: Request, res: Response) => {
    const idi = req.params.id
    console.log('Hello from get id: ', idi)
    const student = students.find(item => item.id === idi)
    if (student === undefined) {
        return res.status(404).send('Error 404, nyt ei ollut oikea student id.')
    }
    res.send(student)
})

server.get('/students', (_req: Request, res: Response) => {
    //console.log(students)
    const studentIds = students.map(student => student.id)
    res.send({ studentIds })
})

server.use(unknownEndpoint)

server.listen(7007)

//tätä tehtävää tehtiin ryhmässä