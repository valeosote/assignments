/*Assignment 8.11: Books API
Create an express REST-API, which offers CRUD for your books. It should include following endpoinst

GET /api/v1/books → Returns a list of all the books
GET /api/v1/books/{id} → Returns a book with a corresponding ID.
POST /api/v1/books → Creates a new book.
PUT /api/v1/books/{id} → Modifies an existing book
DELETE /api/v1/books/{id} → Removes a book with a corresponding id

Every book should have four parameters

id (number)
name (string)
author (string)
read (boolean)

Note: You don’t need to enable an actual database at this point.
Simple runtime-cache (For example: let books = []) will do for now.
Do not save to a file, as we might end up clouding this application later

Hint: Remember to use nodemon as dev-dependency to aid you in the development.*/

import express, {Request, Response, NextFunction} from 'express'

const server = express()
server.use(express.json())

interface Book {
    id: number
    name: string
    author: string
    read: boolean
}

const books: Book[] = []

const validate = (req: Request, res: Response, next: NextFunction) => {
    // req.body voi olla undefined, jolloin allaoleva heittää TypeErrorin
    // tarkista tämä etukäteen, tai käytä ?-notaatiota req.body?.id etc.
    const book: Book = { id: Number(req.body.id), name: req.body.name, author: req.body.author, read: Boolean(req.body.read)} // tässä toi Book tyyppi ei ole tarpeen (ei siitä haittaakaan ole)
    if (typeof(book.id) !== 'number' ||
        typeof(book.name) !== 'string' ||
        typeof(book.author) !== 'string' ||
        typeof(book.read) !== 'boolean') {
        return res.status(400).send({error: 'Error 400. Not all values were provided.'})
    }
    if (isNaN(Number(book.id))) {
        return res.status(400).send({error: 'Error 400. Book id is not a valid number.'})
    }
    next()
}

const validateId = (req: Request, res: Response, next: NextFunction) => {
    const idi = Number(req.params.id)
    if (isNaN(idi)) {
        return res.status(400).send({error: 'Error 400. Given id is not a valid number.'})
    }
    next()
}

//POST /api/v1/books → Creates a new book.
server.post('/api/v1/books', validate, (req: Request, res: Response) => {
    const body: Book = req.body
    books.push(body)
    res.sendStatus(201)
})

//GET /api/v1/books → Returns a list of all the books
server.get('/api/v1/books', (_req: Request, res: Response) => {
    res.send(books)
})

//GET /api/v1/books/{id} → Returns a book with a corresponding ID.
server.get('/api/v1/books/:id', validateId, (req: Request, res: Response) => {
    const idi = Number(req.params.id)
    const book = books.find(book => Number(book.id) === idi)
    if (book === undefined) {
        return res.status(404).send({error: 'Error 404. Invalid book id.'})
    }
    res.send(book)
})

//PUT /api/v1/books/{id} → Modifies an existing book
server.put('/api/v1/books/:id', validateId, (req: Request, res: Response) => {
    const idi = Number(req.params.id)
    const { name, author, read } = req.body
    const book = books.find(book => Number(book.id) === idi)
    if (book === undefined) {
        return res.status(404).send({error: 'Error 404. Invalid book id. No changes applied.'})
    }
    if (!name && !author && typeof(read) !== 'boolean') {
        return res.status(400).send({error: 'Error 400. Please provide at least one attribute to alter.'})
    }
    if (typeof(name) === 'string') {book.name = name}
    if (typeof(author) === 'string') {book.author = author}
    if (typeof(read) === 'boolean') {book.read = read} //tämä ei toimi tällä hetkellä
    res.sendStatus(204)
})

//DELETE /api/v1/books/{id} → Removes a book with a corresponding id
server.delete('/api/v1/books/:id', validateId, (req: Request, res: Response) => {
    const idi = Number(req.params.id) // miksi se on idi eikä id?
    const bookIndex = books.findIndex(book => Number(book.id) === idi)

    if (bookIndex === -1) {
        return res.status(404).send({error: 'Error 404. Could not locate book to remove.'})
    }
    
    books.splice(bookIndex,1)

    // tässäkin kannattaa käyttää moderneja array-metodeja
    // const book = books.find(book => book.id === idi)
    // if (book === undefined) {...}
    // books = books.filter(book => book.id !== idi)
    res.sendStatus(204)
})

server.listen(4400, () => {
    console.log('Listening to port 4400.')
})

/*
{
    "id": "10",
    "name": "The Lord of the Rings",
    "author": "J.R.R. Tolkien",
    "read": "true"
}
*/