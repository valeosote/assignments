/* Middleware for Assignment 8.10 */

import {Request, Response, NextFunction} from 'express'

export const validate = (req: Request, res: Response, next: NextFunction) => {
    const body = req.body
  
    if (!Array.isArray(body)) {
        return res.status(400).json({error: 'Annapa jokin lista.'}) // kasuaalit virheilmoitukset ftw :)
    } else {
        for (const student of body) {
            if (typeof student.name !== 'string' || typeof student.score !== 'number') {
                return res.status(400).json({ error: 'Annapa parempi lista.' })
            }
        }
    }
  
    next()
}
// hyvä validaattori on tämä

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Eipä löydy täältä.'})
}
