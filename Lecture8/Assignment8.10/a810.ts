/*Assignment 8.10: Score Checker API
Create an API for checking student scores.
The API expects a request body to have an array of student objects,
each with a name and score. The API should calculate the average score,
find the highest scoring student, and calculate the percentage of the
students that had score higher or equal to the average score.
The results should be returned in JSON format.

The API should have a validator middleware that checks that the input
is indeed an array that consists of objects with required fields.
If the input is not valid, the API should return a response with
appropriate status code and an error message. */

import express, { Request, Response } from 'express'
import { validate } from './middleware'

const server = express()
server.use(express.json())

interface Student {
    name: string
    score: number
}

interface Analysis {
    averageScore: number
    highestScorer: Student
    aboveAverage: number
}
// tää on jopa ehkä vähän turha tässä, kun sitä käytetään vaan yhden kerran ja sillonkin just ennen kun data muutetaan stringiksi ja lähetetään asiakkaalle.

server.post('/analyze', validate, (req: Request, res: Response) => {
    const body = req.body
    // Koska validaattorissa on tarkistukset tehty, ehkäpä
    // const students: Array<Student> = req.body
    // tälloin myös kaikki siihen käytetyt array-metodit tuottaa oikean tyyppisiä tuloksia automaattisesti

    const averageScore: number = body.reduce((sum: number, student: Student) => sum + student.score, 0) / body.length

    const highestScorer: Student = body.reduce((maxStudent: Student, student: Student) =>
        (student.score > maxStudent.score ? student : maxStudent), body[0])
    // reduce ottaa oletuksena ekaksi parametriksi listan ensimmäisen elementin, joten sitä ei tarvitse erikseen antaa.

    const listOfAboveAverage: Array<Student> = body.filter((student: Student) => student.score >= averageScore)
    const aboveAverage: number = listOfAboveAverage.length / body.length * 100

    // const analysis:Analysis = {averageScore, highestScorer, aboveAverage}

    // res.send({analysis})
    res.send({ averageScore, highestScorer, aboveAverage })
})
// oikein hyvää listametodien käyttöä

server.listen(5050, () => {
    console.log('Listening to port 5050.')
})

/*
[
    { "name": "Leenu", "score": 60 },
    { "name": "Liinu", "score": 85 },
    { "name": "Tiinu", "score": 80 }
]
*/