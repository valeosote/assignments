/*Assignment 8.7: Body Logging
Enable body parsing in your application.
Modify your logger middleware so that in addition to existing functionality,
it also logs the request body if it exists. */

import express, {Request, Response} from 'express'

import {logger, unknownEndpoint} from './middlewares'

const server = express()
server.use(express.json())

const students: Array<string> = []

server.use(logger)

server.use(unknownEndpoint)

server.post('/student', (req: Request, res: Response) => {
    const body: Body = req.body
    //console.log(body)
	students.push(body)
    res.send(body)
})

server.get('/students', (_req: Request, res: Response) => {
    
    res.send({students})
})

server.listen(7007)