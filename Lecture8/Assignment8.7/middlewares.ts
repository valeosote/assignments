/* Assignment 8.7: Body Logging
Enable body parsing in your application.
Modify your logger middleware so that in addition to existing functionality,
it also logs the request body if it exists.
*/
import express, { Request, Response, NextFunction} from 'express'

export const logger = (req: Request, res: Response, next: NextFunction) => {
    
    const currentTime = new Date().toISOString()
    console.log(`${currentTime} ; ${req.method} ; ${req.url}`)
	console.log(req.body)
    next()
}

/*
export const errorHandler = (error, req: Request, res: Response, next: NextFunction) => {
    console.log(error.message)
    if (error.name === 'SomeUserError') {
        res.status(400).send(error.message)
    }
    next(error)
} */

export const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'Ei löydy oppilaita täältä.'})
}