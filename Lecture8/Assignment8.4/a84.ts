/*Assignment 8.4: Advanced Counter Server

Expand the API from the previous assignment by accepting a name through the counter endpoint: /counter/:name

When entering this endpoint, the server should return the count of how many times this named endpoint has been visited.

For example
Aaron enters /counter/Aaron 🠖 "Aaron was here 1 times"
Aaron enters /counter/Aaron 🠖 "Aaron was here 2 times"
Beatrice enters /counter/Beatrice 🠖 "Beatrice was here 1 times"
Aaron enters /counter/Aaron 🠖 "Aaron was here 3 times"
 */

import express, { Request, Response } from 'express'

const server = express()
let countmetoo: { [name: string]: number } = {}

server.listen(9009, () => {
    console.log('Listening to port 9009 ooh yeah.')
})

server.get('/counter/:name', (req: Request, res: Response) => {
    
    const numero = Number(req.query.number)
    const käynti = Number.isNaN(numero) ? 1 : numero

    const nimi = req.params.name

    if (nimi in countmetoo) {
        countmetoo[nimi] += käynti //ei täysin nollaa käyntejä vaan lisää numeron verran
    } else {
        countmetoo[nimi] = käynti
    }

    res.send(`${nimi} was here ${countmetoo[nimi]} times.`)

    //res.send({visits: countmein})
})
