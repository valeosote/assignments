/*Assignment 8.9: PUT and DELETE
Add two more endpoints to your app:

PUT /student/:id should expect the request body to include student name or email.
If both are missing, the endpoint should return a response with status 400 and an error message.
The endpoint should update an existing student identified by the request parameter id.

DELETE /student/:id should remove a student identified by the request parameter id.

Both endpoints should return 404 if there is no student matching the request parameter id.
On success both endpoint should return an empty response with status code 204. */

import express, {Request, Response, NextFunction} from 'express'

import {logger, unknownEndpoint} from './middleware'

const server = express()
server.use(express.json())

interface Body {
    id: string
    name: string
    email: string
}

let students: Array<Body> = []

server.use(logger)

const validate = (req: Request, res: Response, next: NextFunction ) => {
    const {id, name, email} = req.body
    if (typeof(id) !== 'string' || typeof(name) !== 'string' || typeof(email) !== 'string') {
        return res.status(400).send('Nyt puuttui jotain tai oli vääränlaista.')
    }
    next()
}

const validateNameOrEmail = (req: Request, res: Response, next: NextFunction ) => {
    const {name, email} = req.body
    if (typeof(name) !== 'string' && typeof(email) !== 'string') {
        return res.status(400).send('Nyt ei ollut kaivattua nimeä tai spostia.')
    }
    next()
}

server.post('/student', validate, (req: Request, res: Response) => {
    const body: Body = req.body
    //console.log(body)
    students.push(body)
    res.sendStatus(201)
})

server.get('/student/:id', (req: Request, res: Response) => {
    const id = req.params.id
    console.log('Hello from get id: ', id)
    const student = students.find(item => item.id === id)
    if (student === undefined) {
        return res.status(404).send('Error 404, invalid student id')
    }
    res.send(student)
})

server.get('/students', (_req: Request, res: Response) => {
    //console.log(students)
    const studentIds = students.map(student => student.id)
    res.send({ studentIds })
})

server.put('/student/:id', validateNameOrEmail, (req: Request, res: Response) => {
    const idi = req.params.id
    const { name, email } = req.body
    const student = students.find(student => student.id === idi)

    if (student === undefined) {
        return res.status(404).send('Error 404, eipä löytynyt oppilasta.')
    }

    /*if (!name && !email) {
        return res.status(400).send('Tarvii joko nimen tai spostin.')
    }*/

    if (typeof(name) === 'string') {student.name = name}
    if (typeof(email) === 'string') {student.email = email}

    res.sendStatus(204).send('Eiköhän tässä jotain tapahtunut.')
})

server.delete('/student/:id', (req: Request, res: Response) => {
    const idi = req.params.id
    const student = students.find(student => student.id === idi)

    if (student === undefined) {
        return res.status(404).send('Error 404, eipä löytynyt oppilasta.')
    }
    students = students.filter(student => student.id !== idi)
    res.sendStatus(204).send('Oppilas potkaistu pois.')
})

server.use(unknownEndpoint)

server.listen(7007)