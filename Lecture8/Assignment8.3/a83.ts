/*Assignment 8.3: Counter Server
Create an API that consists of only one endpoint:  /counter

Whenever you enter this endpoint from the browser, it
should respond with a JSON object with information on how many times the endpoint has been accessed.

Extra: Make it possible to set the counter to whichever
integer with a query parameter /counter?number=5 */

import express, { Request, Response } from 'express'

const server = express()
let countmein = 0

server.listen(9009, () => {
    console.log('Listening to port 9009 ooh yeah.')
})

server.get('/counter', (req: Request, res: Response) => {
    
    countmein += 1

    if (req.query.number) {
        countmein = parseInt(req.query.number as string)
    }

    const munOmaJSON = { visits: countmein }

    //res.send(`The counter is now at... ${countmein} .`)

    res.send(munOmaJSON)
})
