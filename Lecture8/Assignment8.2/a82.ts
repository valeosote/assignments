/*Assignment 8.2: Simple Express Server

Create a basic Express application.
The application should respond to GET requests by returns
a response with the following content: "Hello world!".

Extra: Add a new endpoint to your application.
So, in addition to accessing http://localhost:3000,
try to send something back from http://localhost:3000/endpoint2.
*/

import express, { Request, Response } from 'express'

const server = express() // server is of type "Express"

server.listen(9009, () => {
    console.log('Listening to port 9009.')
})

server.get('/', (_req: Request, res: Response) => {
    res.send('Hellou wroldi!')
})

server.get('/endpoint2', (req, res) => {
    res.send('I guess this is something, who knows?')
})
