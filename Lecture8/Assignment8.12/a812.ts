/*Assignment 8.12: Improved Books API
Create validator middleware to POST and PUT endpoints that checks that all the required parameters are present.

Create a 404 Not Found -errorhandler and use it in the application

Create a middleware for logging in the application. Make the logs readable.
req-object contains a lot of needed data for this. Use Google if you need ideas
or suggestions of how to achieve this. Do not use any external packages.

Add helmet to your application’s dependencies and create a basic setup for security.*/

import express, {Request, Response, NextFunction} from 'express'
// helmet puuttuu

const server = express()
server.use(express.json())

interface Book {
    id: number
    name: string
    author: string
    read: boolean
}

const books: Book[] = []

const logger = (req: Request, res: Response, next: NextFunction) => {
    const currentTime = new Date().toISOString()
    console.log(`${currentTime} ; ${req.method} ; ${req.url}`)
    console.log(req.body)
    next()
}
server.use(logger)

const validate = (req: Request, res: Response, next: NextFunction) => {
    // Tää ei oo virhe sun puolelta, mutta food for thought:
    // Tässähän me ei vielä oikeestaan tarvita tota Book-tyyppiä ollenkaan, me vaan tarkistetaan parametreja. Joten vois vaihtaa syntaksiksi
    // const { id, name, author, read } = req.body
    // ja sit tarkistaa näiden tyypit kuten oot tossa tehnyt
    // if (id === undefined) {...} etc.


    const book: Book = {id: Number(req.body.id), name: req.body.name, author: req.body.author, read: Boolean(req.body.read)}
    if (book.id === undefined || book.name === undefined || book.author === undefined || book.read === undefined) {
        return res.status(400).json({error: 'Error 400. Not all values were provided.'})
    }
    if (typeof(book.id) !== 'number' ||
        typeof(book.name) !== 'string' ||
        typeof(book.author) !== 'string' ||
        typeof(book.read) !== 'boolean') {
        return res.status(400).send({error: 'Error 400. The values were not of the required types.'})
    } // tämä tais jäädä turhaksi // eivät ole turhia, meille ei riitä vielä että ne on olemassa, vaan pitää myös tarkistaa että niidentyyppi on oikein. Itse asiassa saman asian voi tarkistaa käyttämällä vaan näitä tarkistuksia, sillä jos typeof id === 'number' niin silloin se ei voi olla undefined.

    if (isNaN(Number(book.id))) {
        return res.status(400).send({error: 'Error 400. Book id is not a valid number.'})
    }
    next()
}//heh, tein tätä jo viime tehtävän aikana // mä huomasin, hyvin tehty

const validateId = (req: Request, res: Response, next: NextFunction) => {
    const idi = Number(req.params.id)
    if (isNaN(idi)) {
        return res.status(400).send({error: 'Error 400. Given id is not a valid number.'})
    }
    next()
}

//POST /api/v1/books → Creates a new book.
server.post('/api/v1/books', validate, (req: Request, res: Response) => {
    //const book: Book = req.body
    const book: Book = {id: Number(req.body.id), name: req.body.name, author: req.body.author, read: Boolean(req.body.read)}
    books.push(book)
    res.sendStatus(201)
})

//GET /api/v1/books → Returns a list of all the books
server.get('/api/v1/books', (_req: Request, res: Response) => {
    res.send(books)
})

//GET /api/v1/books/{id} → Returns a book with a corresponding ID.
server.get('/api/v1/books/:id', validateId, (req: Request, res: Response) => {
    const idi = Number(req.params.id)
    const book = books.find(book => Number(book.id) === idi)
    if (book === undefined) {
        return res.status(404).send({error: 'Error 404. Invalid book id.'})
    }
    res.send(book)
})

//PUT /api/v1/books/{id} → Modifies an existing book
server.put('/api/v1/books/:id', validateId, (req: Request, res: Response) => {
    const idi = Number(req.params.id)
    const { name, author, read } = req.body
    const book = books.find(book => Number(book.id) === idi)
    if (book === undefined) {
        return res.status(404).send({error: 'Error 404. Invalid book id. No changes applied.'})
    }
    if (!name && !author && read !== 'true' && read !== 'false') {
        return res.status(400).send({error: 'Error 400. Please provide at least one attribute to alter.'})
    }
    if (typeof(name) === 'string') {book.name = name}
    if (typeof(author) === 'string') {book.author = author}
    if (read === 'true' || read === 'false') {book.read = (read === 'true')}
    res.sendStatus(204)
})

//DELETE /api/v1/books/{id} → Removes a book with a corresponding id
server.delete('/api/v1/books/:id', validateId, (req: Request, res: Response) => {
    const idi = Number(req.params.id)
    const bookIndex = books.findIndex(book => Number(book.id) === idi)
    if (bookIndex === -1) {
        return res.status(404).send({error: 'Error 404. Could not locate book to remove.'})
    }
    books.splice(bookIndex,1)
    res.sendStatus(204)
})

const unknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: 'No such address found.'})
}
server.use(unknownEndpoint)

server.listen(4400, () => {
    console.log('Listening to port 4400.')
})

/*
{
    "id": "10",
    "name": "The Lord of the Rings",
    "author": "J.R.R. Tolkien",
    "read": "true"
}
*/