/*Assignment 6.6: Largest Number
Create a function that finds the largest number in an array.
Create a function that finds the second largest number in an array.
Do this assignment without first sorting the array, or using any functions from the Math module or external libraries. */

function big(lista: Array<number>) {
    let maks = lista[0]
    lista.forEach( n => maks = n > maks ? n : maks)
    return maks
    // tässä halutaan listasta yksi arvo, joka riippuu kaikista listan elementeistä --> reduce
}

function biggest(lista: Array<number>) {
    let maks = lista[0] > lista[1] ? lista[0] : lista[1] //oletan, että annetussa listassa on ainakin kaksi lukua
    let toks = lista[0] + lista[1] - maks
    // voit alustaa vaan molemmat -Infinityyn ja käydä koko listan läpi alusta alkaen, se on lukijalle selkeempää kuin miettiä, että mikä logiikka tässä alkuarvojen valinnassa on

    for (let n = 2; n < lista.length; n++ ) {
        if (lista[n] > maks) {
            toks = maks
            maks = lista[n]
        } else if (lista[n] > toks) {
            toks = lista[n]
        }
    }
    return toks
}

const testejä = [ [1,2,3,4,5],
                  [5,5,5,5,5],
                  [3,2,1,2,3],
                  [-1,-3,-6,-8,-456,-3,-3],
                  [200_000,20_000],
                  [-1,0,1]]

testejä.forEach( nrot => console.log(`${nrot} -> maksimi on ${big(nrot)} ja sen jälkeen ${biggest(nrot)}.`))

// Ruvetaas käyttämään vähän selkeämmin nimettyjä muutujia kuin big, biggest (joka palauttaa toisiksi suurimman), maks ja toks.