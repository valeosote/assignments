/* Assignment 6.3: Random Async (VERSION: async & await)
Use the following asynchronous function twice to get 2 random values. After getting both values, console.log() them.

const getValue = function (): Promise<number> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}

Do this exercise twice. First time, use async & await, and on the second time use promise.then(). */

const getValue = function (): Promise<number> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}

async function kaksinKauniimpi() {
    const ekaluku = await getValue()
    console.log('eka: ' + ekaluku)
    const tokaluku = await getValue()
    console.log('toka: ' + tokaluku)
}

console.log('hep')
kaksinKauniimpi()


//export default getValue


// tässä tosiaan ymmärsin tehtävänannon vähän eri tavalla, koska uskoin odostusaikojen yhdistymisen olevan epätoivottua
// (sinänsä hauska kun eka tein tarkoitetulla tavalla ja sit menin muuttamaan)
// (korjaus tässä tapauksessa ei ole suuri, mutta Promise-versiossa olisi isompi ero)
