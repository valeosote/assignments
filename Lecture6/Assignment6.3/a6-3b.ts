/* Assignment 6.3: Random Async (VERSION: promise.then)
Use the following asynchronous function twice to get 2 random values. After getting both values, console.log() them.

const getValue = function (): Promise<number> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}

Do this exercise twice. First time, use async & await, and on the second time use promise.then(). */

//import getValue from './a6-3'
//tämä import ei toiminutkaan ihan pelkästään näin, niin nimesin tuon getValue vähän eri tavalla tässä

const getValve = function (): Promise<number> { // ehkä getValue?
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}

console.log('jep')

getValve()
    .then( (ekanro) => {
        console.log(ekanro)
        return getValve()
    }).then( (tokanro) => {
        console.log(tokanro)
    })

// eli tosiaan tässä versiossa tein tulostuksen erikseen. tuon ekanro toki saa jotenkin kommunikoitua eteenpäin korjaukseksi