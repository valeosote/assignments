/*Assignment 6.8: Todos
Use the axios library (or fetch) to fetch data from ‘https://jsonplaceholder.typicode.com/todos/’

Console log out that data
Modify the existing data by also fetching the user from ‘https://jsonplaceholder.typicode.com/users/${userId}’ and
adding it to the post where the userId is from, and remove the userId from that data item
Modify in the resulting array of objects the ‘user’ field of every object to only contain the fields ‘name’, ‘username’, and ‘email’.
Modify the functionality so that you separately fetch all users from 'https://jsonplaceholder.typicode.com/users/' and all todos from 'https://jsonplaceholder.typicode.com/todos/', and solve the tasks 2-4 by using only the data from the two arrays you fetched, effectively reducing the amount of requests to the API to 2.
*/

import axios from 'axios'

interface User {
    id: number
    name: string
    username: string
    email: string
    address: any
    phone: string
    website: string
    company: any
}

interface User2 {
    name: string
    username: string
    email: string
}

interface Todo1 { // why is it Todo1 if there is no Todo2?
    userId: number
    id: number
    title: string
    completed: boolean
    user?: User | User2
}

async function superfun() { // superfun? but it has nothing to do with super as in inheritance? why is it superfun?
    const url = `https://jsonplaceholder.typicode.com/todos/`
    const result = await axios.get(url)
    const stuff: Array<Todo1> = result.data
    console.log(stuff)
    return stuff
}

async function userfun() { // getUsers would be industry standard for a function that fetches some User objects
    const url = `https://jsonplaceholder.typicode.com/users/`
    const result = await axios.get(url)
    const users: Array<User> = result.data
    //console.log(users)
    return users
}

async function main() {
    const stuff = await superfun() // stuff is an array of Todo objects, so instead of "stuff", use "todos" as you do on the next line
    const users = await userfun()

//  const users2: Array<User2> = users.map(hlö => ({name: hlö.name, username: hlö.username, email: hlö.email}))
//  stuff.forEach(item => item.user = (for hlö in users.find(iten => iten.id === item.userId) => ({name: hlö.name, username: hlö.username, email: hlö.email}))

    stuff.forEach(item => { // todos.forEach(todo => ...)
        const user = users.find(user => user.id === item.userId)
        if (user) {
          item.user = {name: user.name, username: user.username, email: user.email}
        }
      })

    console.log(stuff[40])
}

main()
