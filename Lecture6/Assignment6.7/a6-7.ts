/*Assignment 6.7: Student Grades
Find the highest, and the lowest scoring students. Then find the average score of the students. Print out only the students who scored higher than the average.
Assign grades (1-5) to all students based on their scores "1": "1-39", "2": "40-59", "3": "60-79", "4": "80-94", "5": "95-100"
*/

interface Student {
    name: string
    score: number
    grade?: number
}

const students: Student[] = [
    { name: 'Markku', score: 99 },
    { name: 'Karoliina', score: 58 },
    { name: 'Susanna', score: 69 },
    { name: 'Benjamin', score: 77 },
    { name: 'Isak', score: 49 },
    { name: 'Liisa', score: 89 },
]

let suurin = students[0]
let pienin = students[0]
let keskiarvo = students[0].score
// Täälläkin mielummin alustaisin neutraalit alkuarvot ja kävisin koko listan läpi.

for (let i = 1; i < students.length; i++) {
    suurin = students[i].score > suurin.score ? students[i] : suurin
    pienin = students[i].score < pienin.score ? students[i] : pienin
    keskiarvo += (students[i].score - keskiarvo) / (i + 1)
}

console.log(`A highest scoring student was ${suurin.name} with a score of ${suurin.score}.`)
console.log(`A lowest scoring student was ${pienin.name} with a score of ${pienin.score}.`)
console.log(`The average of the scores was ${keskiarvo}.`)
console.log(`The students who scored above the average were ${students.filter(opp => opp.score > keskiarvo).map(opp => opp.name).join(', ')}.`)

function arvosana(pisteet: number) {
    if (pisteet >= 95) {
        return 5
    } else if (pisteet >= 80) {
        return 4
    } else if (pisteet >= 60) {
        return 3
    } else if (pisteet >= 40) {
        return 2
    } else if (pisteet >= 1) {
        return 1
    }
    return 0
}

students.forEach(opp => opp.grade = arvosana(opp.score)) // kannattaa nimetä students.forEach(student => ... )
// const gradedStudents = students.map(student => ({ ...student, grade: arvosana(student.score) })) // immutaabeli ratkaisu (ei muokkaa alkuperäistä dataa)

console.log(students)