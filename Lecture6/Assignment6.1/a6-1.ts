/* Assignment 6.1: Callback Countdown
Create a countdown program using the setTimeout function and callbacks.

The program should output something like this.

3   ⇒ Wait 1 second
2   ⇒ Wait another second
1   ⇒ Wait the last 1 second..
GO! */

/*
console.log(3)
setTimeout( () => {
    console.log(2)
    setTimeout( () => {
        console.log(1)
        setTimeout( () => {
            console.log('Go!')
        }, 1000)
    }, 1000)
}, 1000)
*/

//Okei, nyt on testattu miten toimii noin, sit lisätään callbackejä omalla funktiolla...
// Piti laittaa kommenteiksi osa, ettei tulleet tekstit yhtäaikaa.
// Yllä oleva kommentti toimii, alla oleva kommentti ei (se on vain yritys jota en halunnut tuhota).

/*
const munFunktio = function (arg: number, func: () => void) {
    setTimeout(() => {
        func()
    }, arg)
}

console.log('\n3')
munFunktio(1000, () => console.log(2))
munFunktio(1000, () => console.log(1))
munFunktio(1000, () => console.log('Go!'))

Tämä ei toiminut :-P
*/

const munFunktio = function (arg: number, func: () => void) {
    setTimeout(() => {
        func()
    }, arg)
}

console.log('\n3')
munFunktio(1000, () => {
    console.log(2)
    munFunktio(1000, () => {
        console.log(1)
        munFunktio(1000, () => {
            console.log('Go!')
        })
    })
})

