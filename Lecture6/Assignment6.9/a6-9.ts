/* Assignment 6.9: Race Cars
Program a race car simulator!

In this race we have several drivers and several laps.
Each lap every driver has a three percent chance of crashing.
If they don't crash, they finish one lap in 20-25 seconds.
Write a function raceLap that takes no parameters and returns a promise.
If the driver crashes, the promise is rejected.
If not, the promise is resolved and the lap time is returned.

Write a function race that takes two parameters:
a list of drivers (strings), and the number of laps (number).
When the race function is run, it should run the raceLap function for each driver on every lap.
The race function should keep track of each drivers total time and best lap time.
If a driver crashes, their times are not updated on further laps.
When all laps have finished, the race function should return the name and stats of the winner.*/


function raceLip() {
    return new Promise((resolve, reject) => {
        if (Math.random() > 0.03) {
            resolve(20 + Math.random() * 5)
        } else {
            reject(new Error('Oh no! Crash!'))
        }
    })
}

async function rice(ajajat: Array<string>, kierrokset: number) {
    const kisaTulos = ajajat.map(ajajaNimi => ({ name: ajajaNimi, kokonaisAika: 0, parasKierros: Infinity, ulkona: false }))

    for (let i = 0; i < kierrokset; i++) {
        for (const ajajaTulos of kisaTulos) {
            if (!ajajaTulos.ulkona) {
                await raceLip()
                    .then(kierrosAika => {
                        ajajaTulos.kokonaisAika += Number(kierrosAika)
                        if (Number(kierrosAika) < ajajaTulos.parasKierros) {
                            ajajaTulos.parasKierros = Number(kierrosAika)
                        }
                    }).catch(() => {
                        ajajaTulos.ulkona = true
                    })
            }
        }
    }
    let voittaja = null
    let parasAika = Infinity

    for (const ajajaTulos of kisaTulos) {
        if (!ajajaTulos.ulkona && ajajaTulos.kokonaisAika < parasAika) {
            voittaja = ajajaTulos
            parasAika = ajajaTulos.kokonaisAika
        }
    }
    return voittaja
}

const ajajia: string[] = ['Räikkönen','Häkkinen','Lehto','Bottas'];

(async () => {console.log('Voiton vei:', await rice(ajajia, 8))})()