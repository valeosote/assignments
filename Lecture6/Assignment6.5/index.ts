/* Assignment 6.5: Movie Search
Create a movie search function that uses Open Movie Database API. The function should take two parameters, a mandatory title (string) and an optional  year (number).

The movie search should use Axios to make an API call and submit those parameters. The function should use an interface to cast the result to a list of well-typed movie objects. You can choose not to use all the parameters the response has.

Finally, the function should print the results.

You can use api key   c3a0092f . */

import axios from 'axios'

interface Movie {
    Title: string
    Year: string
    //Rated: string
    //Runtime: string
    //ImdbRating: string //näitä ei ollut tarjolla tässä search versiossa
}

async function getMovie(name: string, year?: number){
    const movie : Movie[] = (await axios.get("http://www.omdbapi.com/?apikey=c3a0092f&s="+name+"&y="+year)).data.Search
    movie.forEach( film => console.log('Title: ', film.Title, 'Year: ', film.Year))
    
    //movie.forEach( film => console.log('Title: ', film.Title, 'Year: ', film.Year, 'Rated: ', film.Rated, 'Runtime: ', film.Runtime, 'ImdbRating: ', film.ImdbRating))
    //console.log(movie)
}

getMovie("Shrek",2004)




//axios.get('https://cataas.com/cat')
//    .then(response => console.log(response.data.id))