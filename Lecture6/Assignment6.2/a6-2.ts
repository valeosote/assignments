/* Assignment 6.2: Promise Countdown
Create a countdown program using the setTimeout function and promises.

The program should output something like this.

3 ⇒ Wait 1 second
2 ⇒ Wait another second
1 ⇒ Wait the last 1 second..
GO!*/

console.log(3)

new Promise( (resolve, reject) => {
    setTimeout(resolve,1000)
    reject // tämä ei ole funktiokutsu (siitä puuttuu sulkeet), joten tämä promise ei rejectaa, se vain toteaa että tämmönen funktio tässä möllöttää
    // jos siellä olis funktiokutsu, niin se rejectais sen sillä aikaa kun tuo timeout olis taustalla, eli tää menis heti reject-haaraan eikä koskaan resolvais
}).then( () => {
    console.log(2)
    new Promise( (resolve2, reject2) => {
        setTimeout(resolve2,1000)
        reject2 // sama täällä, nää rejectit on turhia kaikissa tässä tehtävässä
    }).then( () => {
        console.log(1)
        new Promise( (resolve3, reject3) => {
            setTimeout(resolve3,1000)
            reject3
        }).then( () => {
            console.log('Go!')
        }).catch( () => {
            console.log('eipä onnistunu (3)')
        })
    }).catch( () => {
        console.log('eipä onnistunu (2)')
    })
}).catch( () => {
    console.log('eipä onnistunu')
})

//kirjasin näitä reject kohtia muodon vuoksi vaikkei niitä tässä tarvita
//en ole ihan varma tät kirjoittaessa miten näitä .then-juttuja ketjuttaa ilman uusia lupauksia (siis kun tässä ne on vielä tavallaan sisäkkäin)