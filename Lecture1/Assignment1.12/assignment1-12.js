/* Create a program that tells how many groups can be formed if the total number of people and the group size is known. 

The program should take two numbers as command line parameters. The first number is the total number of people, and the second
number is the group size.

If people can not be divided evenly to the groups, one group can have less people than others, but every other group must
have the demanded amount of people.

For example, if the program is ran with 8 people and group size of 4: ```node group_sizes.js 8 4``` → the program should print
"Number of groups: 2".

If there are not enough people for full group, the smaller group should still count as a group in the total
```node group_sizes.js 11 3``` → "Number of groups: 4"

**Extra**: Do this *without* using **if**, or any other conditional operator, or any external libraries.
 */

const aString = process.argv[2] || "100";
const bString = process.argv[3] || "7";

//console.log(aString + " " + bString);

const peopleCount = parseInt(aString);
const groupSize = parseInt(bString);

const groupCount = parseInt( (peopleCount + groupSize -1) / groupSize );

console.log(`Number of groups: ${groupCount}.`)

//toimii parhaiten ei-negatiivisilla luvuilla
//tämä nyt oli tällainen matemaatikolle laadittu tehtävä 

// Oikein hyvältä näyttää tehtävät