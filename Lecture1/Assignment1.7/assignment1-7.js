/* Create variables for **price**, for price in euros, and **discount**, for a discount percentage, and assign some values for those.
Calculate and console.log the *original price*, *discount percentage*, and the *discounted price*.
 */

const price = 99.95;
const discount = 20;

console.log(`The original price was ${price}.`);
console.log(`The discount percentage is ${discount}.`)


const discountedPrice = price * (100 - discount) / 100;

console.log(`The discounted price is ${discountedPrice}.`);

// Tämä ei kylläkään esitä tulosta tunnistettavasti euroina, eli rahayksikön muodossa.