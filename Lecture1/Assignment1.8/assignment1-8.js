/* Create variables for **distance** (kilometers) and **speed** (km / h). Calculate and console.log *travel time*. */

const distance = 250;
const speed = 50;

const travelTime = distance/speed;

console.log(travelTime);