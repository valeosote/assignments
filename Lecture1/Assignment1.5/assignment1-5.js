//console.log("moi");

/* if (console.log('wtf?')) {
    console.log('true')
} else {
    console.log('false')
}*/

const playerCount = 4;

if (playerCount === 4) {
    console.log('Hearts can be played.')
} else {
    console.log(`Hearts can't be played.`)
}

const isStressed = false;
const hasIcecream = true;

if ( !isStressed || hasIcecream ) {
    console.log(`Mark is happy.`)
} else {
    console.log(`Mark is not happy.`)
}

const isShining = true;
const isRaining = false;
const temperature = 30;

if ( isShining && !isRaining && ( temperature > 20) ) {
    console.log(`It is a beach day.`)
} else {
    console.log(`It is not a beach day.`)
}

const seesSuzy = true;
const seesDan = true;
const itsTuesday = true;

if ( (seesSuzy || seesDan) && !(seesSuzy && seesDan) && itsTuesday ) {
    console.log(`Arin is happy.`)
} else {
    console.log(`Arin is sad.`)
}

//D by Ilkka:
let arinHappy = true
const isTuesday = true
const seenSusy = false
const seenDan = true

if(seenSusy === seenDan && isTuesday){
    arinHappy = false
    console.log(arinHappy)
}else{
    arinHappy = true
    console.log(arinHappy)
}

