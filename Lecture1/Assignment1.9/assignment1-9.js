/* Calculate how many seconds there are in a year. Use variables for **days, hours, minutes and seconds in a year**.
Print out the result with console.log. */ //alkuperäinen ohje: 'minutes' tilalla oli 'seconds'

const daysInNonLeapYear = 365;
const hoursInDay = 24;
const minutesInHour = 60;
const secondsInMinute = 60;

const secondsInNonLeapYear = daysInNonLeapYear*hoursInDay*minutesInHour*secondsInMinute;

console.log(`There are ${secondsInNonLeapYear} seconds in a year that is not a leap year, like 2023.`)