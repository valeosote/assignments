
import { convert } from '../src/convert'

test ('dummy test', () => {
    expect(true).toBe(true)
})
// tää testi kannattaa poistaa kun rupee oikeita testejä tekemään ja tietää että mekaniikka toimii

test ('convert dl to l', () => {
    expect(convert(100, 'dl', 'l')).toBe(10)
})

test ('convert l to dl', () => {
    expect(convert(100, 'l', 'dl')).toBe(1000)
})

test ('convert ounces to deciliters', () => {
    expect(convert(100, 'oz', 'dl')).toBe(29.573549417401075)
})

test ('convert liters to cups', () => {
    expect(convert(0.01, 'l', 'c')).toBe(0.042267)
})

test ('convert liters to pints', () => {
    expect(convert(100, 'l', 'pt')).toBe(175.97)
})
