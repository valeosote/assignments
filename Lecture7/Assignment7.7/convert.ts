/*Assignment 7.7: Command Line Converter
Write a command line unit converter that converts between volume units.
Your converter should accept at least units deciliter, liter, ounce, cup, and pint.

The program takes three parameters: amount, source unit, and the target unit.
For example
convert 6 dl oz --> 20

Write tests for your converter. Include at least five tests!*/

const lkmi: number = parseFloat(process.argv[2])
const suniti: string = process.argv[3]
const tuniti: string = process.argv[4]

export function convert(lkm: number, sunit: string, tunit:string): number {
    const muunnos: Record<string, number> = {
        'dl': 10,
        'l': 1,
        'oz': 33.814,
        'c': 4.2267,
        'pt': 1.7597
    }
    return lkm * muunnos[tunit] / muunnos[sunit]
}
console.log(`${convert(lkmi,suniti,tuniti)}`)