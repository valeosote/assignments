
import { calculator } from "../src/index"

test ('dummy test', () => {
    expect(true).toBe(true)
})

test('multiplication', () => {
    expect(calculator('*',2,3)).toBe(6)
})

test('multiplication0', () => {
    expect(calculator('*',0,3)).toBe(0)
})
/*
test('multiplicationI', () => {
    expect(calculator('*',2,Infinity)).toBe(Infinity)
})*/

test('multiplicationD', () => {
    expect(calculator('*',0.2,0.3)).toBe(0.06)
})