import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

export async function miikaFunctionTeht71(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    //const name = request.query.get('name') || await request.text() || 'world';
    
    const inp1 = request.query.get('input') || 'etköantanutkaanmitään'
    const unp2 = inp1.toUpperCase()

    return { body: `You gave the input ${unp2}!` };
};

app.http('miikaFunctionTeht71', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: miikaFunctionTeht71
});
