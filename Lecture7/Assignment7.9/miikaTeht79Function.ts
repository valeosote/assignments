import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

export async function miikaTeht79Function(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`)

    //const name = request.query.get('name') || await request.text() || 'world'

    const min: number = parseFloat(request.query.get('min'))
    const maks: number = parseFloat(request.query.get('max'))
    const kokonainenko: boolean = request.query.get('int') === 'true' ? true : false

    const satunnainen = kokonainenko ?
        Math.floor(Math.random() * (maks - min) + min) :
        Math.random() * (maks - min) + min

    //console.log(satunnainen)

    return { body: `${satunnainen}` }
}

app.http('miikaTeht79Function', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: miikaTeht79Function
})
