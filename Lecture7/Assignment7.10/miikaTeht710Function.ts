import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";
import axios from 'axios'

interface Cat {
    tags: Array<string>
    createdAt: string
    updatedAt: string
    mimetype: string
    size: number
    _id: string
}

interface Dog {
    message: string
    status: string
}

interface Fox {
    image: string
    link: string
}

async function giveMeACat() {
    //return 'https://cataas.com/cat/zdqpqHPRMMMr429Y'

    const cat: Cat = (await axios.get('https://cataas.com/cat?json=true')).data

    return 'https://cataas.com/cat/' + cat._id
}

async function giveMeADog() {
        //return 'https:\/\/images.dog.ceo\/breeds\/terrier-scottish\/n02097298_3342.jpg'

        const dog: Dog = (await axios.get('https://dog.ceo/api/breeds/image/random')).data

        return dog.message
}

async function giveMeAFox() {
    //return 'https:\/\/randomfox.ca\/?i=117'

    const fox: Fox = (await axios.get('https://randomfox.ca/floof')).data

    return fox.image
}

let laskuri = 0

export async function miikaTeht710Function(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`)

    //const name = request.query.get('name') || await request.text() || 'world';

    laskuri += 1
    let linkki = ''
    let elain = ''

    if (laskuri % 13 === 0) {
        linkki = await giveMeAFox()
        elain = 'fox ;-)'
    } else if (Math.random() > 0.5) {
        linkki = await giveMeACat()
        elain = 'cat'
    } else {
        linkki = await giveMeADog()
        elain = 'dog'
    }

    //return { message: `Here, enjoy your picture of a ${elain}.`, link: linkki }

    const htmlResult = `
        <!DOCTYPE html>
        <html>
        <head>
            <title>Enjoy Your Picture</title>
        </head>
        <body>
            <h1>Here, enjoy your picture of a ${elain}.</h1>
            <img src="${linkki}" alt="${elain}">
        </body>
        </html>`

    return { 
        headers: {"Content-Type": "text/html"},
        body: htmlResult
      }
}

app.http('miikaTeht710Function', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: miikaTeht710Function
})

// Hyvältä näyttää!