
import { calculator } from '../src/calculator'

test ('dummy test', () => {
    expect(true).toBe(true)
})

test('multiplication', () => {
    expect(calculator('*',2,3)).toBe(6)
})

test('multiplication0', () => {
    expect(calculator('*',0,3)).toBe(0)
})

test('multiplicationI', () => {
    expect(calculator('*',2,Infinity)).toBe(Infinity)
})

test('multiplicationD', () => {
    expect(calculator('*',0.2,0.3)).toBe(0.06)
})

describe('jakolaskua', () => {
    it('Returns 2 with parameters 8 and 4', () => {
        const result = calculator('/',8,4)
        expect(result).toBe(2)
    })
    it('Returns 0 with parameters 0 and 4', () => {
        const result = calculator('/',0,4)
        expect(result).toBe(0)
    })
    it('Returns Infinity with parameters 8 and 0', () => {
        const result = calculator('/',8,0)
        expect(result).toBe(Infinity)
    })
    it('Returns 2.5 with parameters 1_000_000 and 400_000', () => {
        const result = calculator('/',1_000_000,400_000)
        expect(result).toBe(2.5)
    })

    it('Returns 0 when calculating 0+0', () => {
        const result = calculator('+',0,0)
        expect(result).toBe(0)
    })
    it('Returns 100 when calculating 47+53', () => {
        const result = calculator('+',47,53)
        expect(result).toBe(100)
    })
    it('Returns 10 when calculating -10+20', () => {
        const result = calculator('+',-10,20)
        expect(result).toBe(10)
    })

    it('Returns 0 when calculating 0-0', () => {
        const result = calculator('-',0,0)
        expect(result).toBe(0)
    })
    it('Returns -1 when calculating 10-11', () => {
        const result = calculator('-',10,11)
        expect(result).toBe(-1)
    })
    it('Returns -Infinity when calculating 0-Infinity', () => {
        const result = calculator('-',0,Infinity)
        expect(result).toBe(-Infinity)
    })

})